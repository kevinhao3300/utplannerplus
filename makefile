start_docker:
	sudo docker build --no-cache -t docker_image .
	sudo docker run -p 80:80 docker_image:latest

run_jtest:
	cd frontend && npm install && npm run test

start_backend:
	cd backend && pip3 install -r requirements.txt && python3 main.py

TESTFILES := `ls backend/tests`

run_backend_tests: 
	for file in $(TESTFILES); do \
        python3 backend/tests/$${file} ; \
    done

