name, EID, and GitLab ID, of all members  
    Kevin Hao, kjh2858, @kevinhao3300  
    Jesse Zou, jmz679, @jessezou    
    Jordan Wang, jyw283, @jwang724  
    Jackson McClurg, jtm3984 @jacksonmcclurg  
    Alexy Correa, ac67826 @acorrea9  
  
Git SHA  
    62c9c353e47196b5014f5ac1fe14b77432c3ced3      
  
Pipeline   
    https://gitlab.com/kevinhao3300/utplannerplus/-/pipelines  
  
project leader  
    Jordan     
  
link to website    
    https://www.utplannerplus.me/  

Phase 1:  
estimated completion time for each member (hours: int)  
    Kevin Hao - 15  
    Jesse Zou - 15  
    Jordan Wang - 10  
    Jackson McClurg - 16  
    Alexy Correa - 8  
  
actual completion time for each member (hours: int)  
    Kevin Hao - 14   
    Jesse Zou - 12   
    Jordan Wang - 12    
    Jackson McClurg - 13    
    Alexy Correa - 11  

------------------------------------------------------------------------------
Phase 2:  
estimated completion time for each member (hours: int)  
    Kevin Hao - 15  
    Jesse Zou - 15  
    Jordan Wang - 15  
    Jackson McClurg - 14  
    Alexy Correa - 15 
  
actual completion time for each member (hours: int)  
    Kevin Hao - 20   
    Jesse Zou - 20   
    Jordan Wang - 20    
    Jackson McClurg - 20    
    Alexy Correa - 17 

------------------------------------------------------------------------------
Phase 3:  
estimated completion time for each member (hours: int)  
    Kevin Hao - 20  
    Jesse Zou - 20  
    Jordan Wang - 15  
    Jackson McClurg - 14  
    Alexy Correa - 15
  
actual completion time for each member (hours: int)  
    Kevin Hao - 30  
    Jesse Zou - 40  
    Jordan Wang - 24  
    Jackson McClurg - 30   
    Alexy Correa - 20

------------------------------------------------------------------------------
Phase 4:  
estimated completion time for each member (hours: int)  
    Kevin Hao - 15  
    Jesse Zou - 16  
    Jordan Wang - 12  
    Jackson McClurg - 10  
    Alexy Correa - 20
  
actual completion time for each member (hours: int)  
    Kevin Hao - 16  
    Jesse Zou - 17   
    Jordan Wang - 14  
    Jackson McClurg - 15   
    Alexy Correa - 32

Final presentation: https://youtu.be/EStkol5OMdY
  
comments  
  
