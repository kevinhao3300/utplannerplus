FROM nikolaik/python-nodejs

RUN git clone https://gitlab.com/kevinhao3300/utplannerplus.git

WORKDIR /utplannerplus

RUN ls && pwd

RUN git pull --force

RUN cd frontend && npm install && npm run build

RUN pip3 install -r backend/requirements.txt

EXPOSE 80

CMD git pull --force && python3 backend/main.py