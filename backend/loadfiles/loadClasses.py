import numpy as np
import main

# load the scraped data
classes = np.load("scrape/classes.npy", allow_pickle=True)
it = iter(classes)

main.db.create_all()
identifier = 0
# populate the class table in the database
try:
    while True:
        d = next(it)
        # creates everything as a String in the db, plan on changing this
        # to more accurately represent the type/improve efficiency
        c = main.Classes(
            identifier,
            str(d.get("Name")),
            str(d.get("Level")),
            str(d.get("Semester")),
            str(d.get("Department")),
            str(int(str(d.get("Unique"))[2:-2])),
            str(d.get("Days")),
            str(d.get("Hour")),
            str(d.get("Room")),
            str(d.get("Instruction Mode")),
            str(d.get("Instructor")),
            str(d.get("Status")),
            str(d.get("Flags")),
        )
        identifier += 1
        main.db.session.add(c)

except StopIteration:
    main.db.session.commit()
    pass
