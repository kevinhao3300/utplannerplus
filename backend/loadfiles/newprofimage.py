from bs4 import BeautifulSoup
import requests
import numpy as np
import main

main.db.create_all()

# school of architecture
# uses beautiful soup to get all images
response = requests.get('https://soa.utexas.edu/about/faculty')
soup = BeautifulSoup(response.text, 'html.parser')
body = soup.findAll("img")

# loops through all prof images
for x in body[5:-11]:
    name = x['alt']
    img = x['src']
    name = name[9:]
    # get name
    s = name.split(" ")
    fullname = s[-1] + ", " + s[0]
    fullname = fullname.upper()
    # find name in prof table
    prof = main.Prof.query.filter(main.Prof.name.contains(fullname)).first()
    # add img url to field
    if prof is not None:
        prof.image = img


# cns
num = 0
# loop through num of pages of faculty
for i in range(45):
    strnum = str(num)
    # uses beautiful soup to get all images
    p1 = 'https://cns.utexas.edu/directory/'
    p2 = 'items/1-directory?start='
    url = p1+p2
    response = requests.get(url + strnum)
    soup = BeautifulSoup(response.text, 'html.parser')
    body = soup.findAll('img')
    # loop through all prof images
    for x in body[14:-8]:
        name = x['alt']
        img = x['src']
        # get only prof names
        if name != "No" and name != "Yes":
            name = name.upper()
            img = 'https://cns.utexas.edu' + img
            # find name in prof table
            prof = main.Prof.query.filter(
                main.Prof.name.contains(name)
            ).first()
            if prof is not None:
                prof.image = img
    num = num + 50

#McCombs
#list of departments in McCombs
list = ['Accounting', 'AIM-Investment-Center', 'BBA-Program-Office', 
'Business-Affairs', 'Business-Government-and-Society',
'Career-Management-and-Corporate-Relations',
'Center-for-Business-Technology-and-Law',
'Center-for-Customer-Insight-and-Marketing-Solution',
'Center-for-Energy-Finance','Center-for-Global-Business',
'Center-for-Leadership-and-Ethics',
'Center-for-Research-in-Electronic-Commerce',
'Center-for-Risk-Mgmt-and-Insurance','Development-and-External-Relations',
'Energy-Management-and-Innovation-Center','Executive-Education-Programs',
'Finance','Financial-Education-and-Research-Center','Health-Care-Initiative',
'Herb-Kelleher-Center-for-Entrepreneurship',
'Hispanic-Leadership-Initiative','HMTF-Center-for-Private-Equity',
'Information-Management','Information-Risk-and-Operations-Mgmt','Management',
'Marketing','Communications','MBA-Program-Office','MSB-Computer-Services',
'MSB-Media-Services','Office-of-the-Dean','Peterson-Research-Initiatives',
'Real-Estate-Center','Recruitment-Services',
'Supply-Chain-Management-Center-of-Excellence',
'Sustainability-and-Social-Innovation-Initiative','Value-Institute']

#loop through each dept
for dept in list:
    # uses beautiful soup to get all images
    p1 = 'https://www.mccombs.utexas.edu/'
    p2 = 'Directory/Departments/'
    url = p1+p2
    response = requests.get(url + dept)
    soup = BeautifulSoup(response.text, 'html.parser')
    body = soup.findAll("img")
    #loop through all prof images
    for x in body[3:-5]:
        name = x['alt']
        img = x['src']
        #filter names
        if name != 'Expand Menu' and 'McCombs' not in name:
            name = name.upper()
            s = name.split(" ")
            fullname = s[-1] + ", " + s[0]
            #find name in prof table
            prof = main.Prof.query.filter(
                main.Prof.name.contains(fullname)
            ).first()
            if prof is not None:
                prof.image = img

#college of education
# uses beautiful soup to get all images
response = requests.get('https://education.utexas.edu/research/find-faculty')
soup = BeautifulSoup(response.text, 'html.parser')
body = soup.findAll("img")

#loop through all prof images
for x in body[2:-1]:
    name = x['alt']
    img = x['src']
    s = name.split(" ")
    #filter prof names
    if len(s) > 2:
        fullname = s[2] + ", " + s[0]
    else:
        fullname = s[1] + ", " + s[0]
    fullname = fullname.upper()
    #find name in prof table
    prof = main.Prof.query.filter(main.Prof.name.contains(fullname)).first()
    if prof is not None:
        prof.image = img

#engineering
# uses beautiful soup to get all images
response = requests.get('https://www.engr.utexas.edu/faculty-directory')
soup = BeautifulSoup(response.text, 'html.parser')
body = soup.findAll("img")
head = 'https://www.engr.utexas.edu'

#loop through all prof images
for x in body[2:-11]:
    name = x['alt']
    img = x['src']
    #filter prof names
    if name != "grey box":
        name = name[9:]
        s = name.split(" ")
        firstname = s[0]
        lastname = s[-1]
        fullname = lastname + ", " + firstname
        fullname = fullname.upper()
        imgurl = head + img
        #find name in prof table
        prof = main.Prof.query.filter(
            main.Prof.name.contains(fullname)
        ).first()
        if prof is not None:
            prof.image = imgurl

#geology
# uses beautiful soup to get all images
response = requests.get('https://www.jsg.utexas.edu/dgs/people/faculty/')
soup = BeautifulSoup(response.text, 'html.parser')
body = soup.findAll("img")
#loop through prof images
for x in body[3:-1]:
    name = x['alt']
    img = x['src']
    #filter prof names
    s = name.split(" ")
    if len(s) > 2:
        fullname = s[2] + ", " + s[0]
    else:
        fullname = s[1] + ", " + s[0]
    fullname = fullname.upper()
    #find name in prof table
    prof = main.Prof.query.filter(main.Prof.name.contains(fullname)).first()
    if prof is not None:
        prof.image = img

#cola
#list of departments in cola
list = ['aads', 'afrotc', 'ams', 'anthropology', 'asianstudies', 
'classics', 'economics', 'english', 'frenchitalian', 'geography', 'germanic',
'government', 'history', 'linguistics', 'philosophy', 'psychology', 'rs', 
'rhetoric', 'slavic', 'sociology']

#loops through each dept
for dept in list:
    # uses beautiful soup to get all images
    response = requests.get(
        'https://liberalarts.utexas.edu/' + dept + '/faculty/')
    soup = BeautifulSoup(response.text, 'html.parser')
    body = soup.findAll("img")
    #loops through all prof images
    for x in body[7:-1]:
        name = x['alt']
        img = x['src']
        #filter prof names
        name = name[9:]
        s = name.split(" ")
        firstname = s[0]
        lastname = s[-1]
        if lastname == 'PhD':
            lastname = s[-2]
            lastname = lastname[0:-1]
        fullname = lastname + ", " + firstname
        fullname = fullname.upper()
        #find name in prof table
        prof = main.Prof.query.filter(
            main.Prof.name.contains(fullname)).first()
        if prof is not None:
            prof.image = img

#nursing
# uses beautiful soup to get all images
response = requests.get('https://nursing.utexas.edu/faculty/search')
soup = BeautifulSoup(response.text, 'html.parser')
body = soup.findAll("img")
#loop through all prof images
for x in body[1:-1]:
    name = x['alt']
    img = x['src']
    #filter the prof images
    x = ("PDF" not in name and "Microsoft" not in name)
    y = ("Default" not in name and "File" not in name and name != "")
    if x and y:
        s = name.split(" ")
        if len(s) > 2:
            fullname = s[2] + ", " + s[0]
        else:
            fullname = s[1] + ", " + s[0]
        fullname = fullname.upper()
        #find name in prof table
        prof = main.Prof.query.filter(
            main.Prof.name.contains(fullname)).first()
        if prof is not None:
            prof.image = img

#pharmacy
# uses beautiful soup to get all images
p1='https://pharmacy.utexas.edu/about/'
p2='contact-us/austin-faculty-staff'
url = p1+p2
response = requests.get(url)
soup = BeautifulSoup(response.text, 'html.parser')
body = soup.findAll("img")
#loop through all prof images
for x in body[2:-2]:
    name = x['alt']
    img = x['src']
    #filter prof name
    name = name.upper()
    if name == "PLACEHOLDER":
        continue
    if "PHOTO OF" in name:
        name = name[9:]
    if "PICTURE OF" in name:
        name = name[11:]
    if "PROFILE PIC" in name:
        name = name[:-12]
    if "HEADSHOT" in name:
        name = name[:-9]
    if "PHOTO" in name:
        name = name[:-6]
    s = name.split(" ")
    if len(s) > 1:
        fullname = s[1] + ", " + s[0]
    else:
        fullname = name
    imgurl = 'https://pharmacy.utexas.edu' + img
    #find name in prof model
    prof = main.Prof.query.filter(main.Prof.name.contains(fullname)).first()
    if prof is not None:
        prof.image = imgurl

# social work
# uses beautiful soup to get all images
response = requests.get('https://socialwork.utexas.edu/academics/faculty/')
soup = BeautifulSoup(response.text, 'html.parser')
body = soup.findAll("img")
#loop through all prof images
for x in body[2:-1]:
    name = x['alt']
    img = x['src']
    #filter prof names
    s = name.split(" ")
    if len(s) > 2:
        for item in s:
            if "," in item:
                lastname = item
                break
        firstname = s[0]
    else:
        lastname = s[1]
        firstname = s[0]
    if "," in lastname:
        lastname = lastname[:-1]
    fullname = lastname + ", " + firstname
    fullname = fullname.upper()
    if "https:" not in img:
        img = "https:" + img
    #find name in prof model
    prof = main.Prof.query.filter(main.Prof.name.contains(fullname)).first()
    if prof is not None:
        prof.image = img

#moody
# uses beautiful soup to get all images
response = requests.get('https://moody.utexas.edu/faculty')
soup = BeautifulSoup(response.text, 'html.parser')
body = soup.findAll("img")
#loop through all prof images
for x in body[2:-2]:
    name = x['alt']
    img = x['src']
    #filter prof names
    s = name.split(" ")
    fullname = s[-1] + ", " + s[0]
    fullname = fullname.upper()
    #find name in prof table
    prof = main.Prof.query.filter(main.Prof.name.contains(fullname)).first()
    if prof is not None:
        prof.image = img

main.db.session.commit()
