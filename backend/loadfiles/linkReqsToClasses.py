import numpy as np
import main 

# link the requirements to classes found as matches

classes = np.load("classes.npy", allow_pickle=True)
main.db.create_all()
# get all requirements
req_search = main.Requirements.query.all()
req_classes = []
# loop through requirements
for req in req_search:
    # get exampleClasses field and split into separate clases
    exclass = req.exampleClasses
    exclass = exclass.split(", ")
    # loop through each class
    for c in exclass:
        # find class in classes table
        class_q = main.Classes.query.filter_by(name=c).all()
        # add the requirement for each class
        if class_q is not None:
            for q in class_q:
                q.req = req.identifier


main.db.session.commit()
