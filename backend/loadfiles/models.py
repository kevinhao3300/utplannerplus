import main
from dataclasses import dataclass
from main import db

''' 
this file contains the class declarations for all of our data models. 
there are three - Class, Prof, and Requirement. below these three 
are three more, which are our old models that are no longer in use, 
but we are hanging onto for reference.
'''
# MOST RECENT DISTINCT CLASSES
@dataclass
class distinctClass(db.Model):
    pkey: int = db.Column(db.Integer, unique=False, nullable=False, primary_key=True)
    name: str = db.Column(db.Text, unique=False, nullable=False)
    level: str = db.Column(db.Text, unique=False, nullable=False)
    semester: str = db.Column(db.Text, unique=False, nullable=False)
    department: str = db.Column(db.Text, unique=False, nullable=False)
    unique: int = db.Column(db.Integer, unique=False, nullable=False)
    days: str = db.Column(db.Text, unique=False, nullable=False)
    hour: str = db.Column(db.Text, unique=False, nullable=False)
    room: str = db.Column(db.Text, unique=False, nullable=False)
    instructionMode: str = db.Column(db.Text, unique=False, nullable=False)
    instructor: str = db.Column(db.Text, unique=False, nullable=False)
    status: str = db.Column(db.Text, unique=False, nullable=False)
    req: str = db.Column(db.Text, unique=False, nullable=False)
    description: str = db.Column(db.Text, unique=False, nullable=False)

    def __init__(
        self,
        identifier,
        name,
        level,
        semester,
        department,
        unique,
        days,
        hour,
        room,
        instructionMode,
        instructor,
        status,
        req="None",
        description="",
    ):
        self.identifier = identifier
        self.name = name
        self.level = level
        self.semester = semester
        self.department = department
        self.unique = unique
        self.days = days
        self.hour = hour
        self.room = room
        self.instructionMode = instructionMode
        self.instructor = instructor
        self.status = status
        self.req = req
        self.description = description

    def __repr__(self):
        return "<Class %r>" % self.name



# CLASS MODEL
@dataclass
class Class(db.Model):
    pkey: int = db.Column(
        db.Integer, unique=False, nullable=False, primary_key=True
    )
    name: str = db.Column(db.Text, unique=False, nullable=False)
    level: str = db.Column(db.Text, unique=False, nullable=False)
    semester: str = db.Column(db.Text, unique=False, nullable=False)
    department: str = db.Column(db.Text, unique=False, nullable=False)
    unique: int = db.Column(db.Integer, unique=False, nullable=False)
    days: str = db.Column(db.Text, unique=False, nullable=False)
    hour: str = db.Column(db.Text, unique=False, nullable=False)
    room: str = db.Column(db.Text, unique=False, nullable=False)
    instructionMode: str = db.Column(db.Text, unique=False, nullable=False)
    instructor: str = db.Column(db.Text, unique=False, nullable=False)
    status: str = db.Column(db.Text, unique=False, nullable=False)
    req: str = db.Column(db.Text, unique=False, nullable=False)
    description: str = db.Column(db.Text, unique=False, nullable=False)

    def __init__(
        self,
        identifier,
        name,
        level,
        semester,
        department,
        unique,
        days,
        hour,
        room,
        instructionMode,
        instructor,
        status,
        req="None",
        description="",
    ):
        self.identifier = identifier
        self.name = name
        self.level = level
        self.semester = semester
        self.department = department
        self.unique = unique
        self.days = days
        self.hour = hour
        self.room = room
        self.instructionMode = instructionMode
        self.instructor = instructor
        self.status = status
        self.req = req
        self.description = description

    def __repr__(self):
        return "<Class %r>" % self.name

# PROF MODEL
@dataclass
class Prof(db.Model):
    pkey: int = db.Column(
        db.Integer, unique=False, nullable=False, primary_key=True
    )
    name: str = db.Column(db.Text, unique=False, nullable=False)
    tID: int = db.Column(db.Integer, unique=False, nullable=False)
    tDept: str = db.Column(db.Text, unique=False, nullable=False)
    rating_class: str = db.Column(db.Text, unique=False, nullable=False)
    overall_rating: str = db.Column(db.Text, unique=False, nullable=False)
    num_ratings: str = db.Column(db.Text, unique=False, nullable=False)
    classes_taught: str = db.Column(db.Text, unique=False, nullable=False)
    image: str = db.Column(db.Text, unique=False, nullable=False)
    wta: str = db.Column(db.Text, unique=False, nullable=False)
    level_of_diff: str = db.Column(db.Text, unique=False, nullable=False)
    tags: str = db.Column(db.Text, unique=False, nullable=False)
    req: str = db.Column(db.Text, unique=False, nullable=False)

    def __init__(
        self,
        p,
        tid=-1,
        name="unknown",
        tDept="unknown",
        rating_class="unknown",
        overall_rating=0,
        num_ratings="N/A",
        classes_taught="",
        image="",
        wta="N/A",
        level_of_diff="N/A",
        tags="",
        req="None",
    ):
        self.id = p
        self.name = name
        self.tID = tid
        self.tDept = tDept
        self.rating_class = rating_class
        self.overall_rating = overall_rating
        self.num_ratings = num_ratings
        self.image = image
        self.wta = wta
        self.level_of_diff = level_of_diff
        self.tags = tags
        self.req = req

# REQUIREMENT MODEL
@dataclass
class Requirement(db.Model):
    pkey: int = db.Column(
        db.Integer, unique=False, nullable=False, primary_key=True
    )
    name: str = db.Column(db.Text, unique=False, nullable=False)
    inResidence: str = db.Column(db.Text, unique=False, nullable=False)
    exampleClasses: str = db.Column(db.Text, unique=False, nullable=False)
    typeOfRequirement: str = db.Column(db.Text, unique=False, nullable=False)
    numHours: int = db.Column(db.Integer, unique=False, nullable=False)
    image: str = db.Column(db.Text, unique=False, nullable=False)
    web: str = db.Column(db.Text, unique=False, nullable=False)
    prof: str = db.Column(db.Text, unique=False, nullable=False)

    def __init__(
        self,
        p,
        name="unknown",
        inResidence="unknown",
        exampleClasses="unkown",
        typeOfRequirement="unknown",
        numHours=0,
        image="",
        web="",
        prof="",
    ):
        self.identifier = p
        self.name = name
        self.inResidence = inResidence
        self.exampleClasses = exampleClasses
        self.typeOfRequirement = typeOfRequirement
        self.numHours = numHours
        self.image = image
        self.web = web
        self.prof = prof

    def __repr__(self):
        return "<Requirement %r>" % self.name


# old models


@dataclass
class Professor(db.Model):
    pkey: int = db.Column(
        db.Integer, unique=False, nullable=False, primary_key=True
    )
    name: str = db.Column(db.String(300), unique=False, nullable=False)
    tID: int = db.Column(db.Integer, unique=False, nullable=False)
    tDept: str = db.Column(db.String(300), unique=False, nullable=False)
    rating_class: str = db.Column(db.String(300), unique=False, nullable=False)
    overall_rating: float = db.Column(db.Float, unique=False, nullable=False)
    num_ratings: float = db.Column(db.Float, unique=False, nullable=False)
    classes_taught: str = db.Column(db.Text, unique=False, nullable=False)
    image: str = db.Column(db.Text, unique=False, nullable=False)

    def __init__(
        self,
        p,
        tid=-1,
        name="unknown",
        tDept="unknown",
        rating_class="unknown",
        overall_rating=0,
        num_ratings=0,
        classes_taught="",
        image="",
    ):
        self.id = p
        self.name = name
        self.tID = tid
        self.tDept = tDept
        self.rating_class = rating_class
        self.overall_rating = overall_rating
        self.num_ratings = num_ratings
        self.image = image


@dataclass
class Classes(db.Model):
    identifier: int = db.Column(
        db.Integer, unique=False, nullable=False, primary_key=True
    )
    name: str = db.Column(db.String(300), unique=False, nullable=False)
    level: str = db.Column(db.String(300), unique=False, nullable=False)
    semester: str = db.Column(db.String(300), unique=False, nullable=False)
    department: str = db.Column(db.String(300), unique=False, nullable=False)
    unique: int = db.Column(db.Integer, unique=False, nullable=False)
    days: str = db.Column(db.String(300), unique=False, nullable=False)
    hour: str = db.Column(db.String(300), unique=False, nullable=False)
    room: str = db.Column(db.String(300), unique=False, nullable=False)
    instructionMode: str = db.Column(
        db.String(300), unique=False, nullable=False
    )
    instructor: str = db.Column(db.String(300), unique=False, nullable=False)
    status: str = db.Column(db.String(300), unique=False, nullable=False)
    flags: str = db.Column(db.String(300), unique=False, nullable=False)
    req: int = db.Column(db.Integer, unique=False, nullable=False)

    def __init__(
        self,
        identifier,
        name,
        level,
        semester,
        department,
        unique,
        days,
        hour,
        room,
        instructionMode,
        instructor,
        status,
        flags,
        req=-1,
    ):
        self.identifier = identifier
        self.name = name
        self.level = level
        self.semester = semester
        self.department = department
        self.unique = unique
        self.days = days
        self.hour = hour
        self.room = room
        self.instructionMode = instructionMode
        self.instructor = instructor
        self.status = status
        self.flags = flags
        self.req = req

    def __repr__(self):
        return "<Class %r>" % self.name


@dataclass
class Requirements(db.Model):
    identifier: int = db.Column(
        db.Integer, unique=False, nullable=False, primary_key=True
    )
    name: str = db.Column(db.String(300), unique=False, nullable=False)
    inResidence: str = db.Column(db.String(80), unique=False, nullable=False)
    exampleClasses: str = db.Column(
        db.String(300), unique=False, nullable=False
    )
    typeOfRequirement: str = db.Column(
        db.String(300), unique=False, nullable=False
    )
    numHours: int = db.Column(db.Integer, unique=False, nullable=False)
    image: str = db.Column(db.Text, unique=False, nullable=False)
    web: str = db.Column(db.Text, unique=False, nullable=False)

    def _init_(
        self,
        p,
        name="unknown",
        inResidence="unknown",
        exampleClasses="unkown",
        typeOfRequirement="unknown",
        numHours=0,
        image="",
        web="",
    ):
        self.identifier = p
        self.name = name
        self.inResidence = inResidence
        self.exampleClasses = exampleClasses
        self.typeOfRequirement = typeOfRequirement
        self.numHours = numHours
        self.image = image
        self.web = web

    def __repr__(self):
        return "<Requirement %r>" % self.name


@dataclass
class Class1(db.Model):
    pkey: int = db.Column(db.Integer, unique=False, nullable=False, primary_key=True)
    name: str = db.Column(db.Text, unique=False, nullable=False)
    number: str = db.Column(db.Text, unique=False, nullable=False)
    level: str = db.Column(db.Text, unique=False, nullable=False)
    semester: str = db.Column(db.Text, unique=False, nullable=False)
    department: str = db.Column(db.Text, unique=False, nullable=False)
    unique: int = db.Column(db.Integer, unique=False, nullable=False)
    days: str = db.Column(db.Text, unique=False, nullable=False)
    hour: str = db.Column(db.Text, unique=False, nullable=False)
    room: str = db.Column(db.Text, unique=False, nullable=False)
    req: str = db.Column(db.Text, unique=False, nullable=False)
    instructionMode: str = db.Column(db.Text, unique=False, nullable=False)
    instructor: str = db.Column(db.Text, unique=False, nullable=False)
    description: str = db.Column(db.Text, unique=False, nullable=False)

    def __init__(
        self,
        p,
        name,
        number,
        level,
        semester,
        department,
        unique,
        days,
        hour,
        room,
        req,
        instructionMode,
        instructor,
        description="",
    ):
        self.pkey = p
        self.name = name
        self.number = number
        self.level = level
        self.semester = semester
        self.department = department
        self.unique = unique
        self.days = days
        self.hour = hour
        self.room = room
        self.req = req
        self.instructionMode = instructionMode
        self.instructor = instructor
        self.description = description

    def __repr__(self):
        return "<Class %r>" % self.name


@dataclass
class Professor1(db.Model):
    pkey: int = db.Column(db.Integer, unique=False, nullable=False, primary_key=True)
    name: str = db.Column(db.Text, unique=False, nullable=False)
    tID: int = db.Column(db.Integer, unique=False, nullable=False)
    department: str = db.Column(db.Text, unique=False, nullable=False)
    ratingClass: str = db.Column(db.Text, unique=False, nullable=False)
    overallRating: str = db.Column(db.Text, unique=False, nullable=False)
    numRatings: str = db.Column(db.Text, unique=False, nullable=False)
    image: str = db.Column(db.Text, unique=False, nullable=False)
    wouldTakeAgain: str = db.Column(db.Text, unique=False, nullable=False)
    levelOfDifficulty: str = db.Column(db.Text, unique=False, nullable=False)
    tags: str = db.Column(db.Text, unique=False, nullable=False)

    def __init__(
        self,
        p,
        name="unknown",
        tid=-1,
        tDept="unknown",
        ratingClass="unknown",
        overallRating="0",
        numRatings="N/A",
        image="",
        wouldTakeAgain="N/A",
        levelOfDifficulty="N/A",
        tags="",
    ):
        self.id = p
        self.name = name
        self.tID = tid
        self.department = tDept
        self.ratingClass = ratingClass
        self.overallRating = overallRating
        self.numRatings = numRatings
        self.image = image
        self.wouldTakeAgain = wouldTakeAgain
        self.levelOfDifficulty = levelOfDifficulty
        self.tags = tags


@dataclass
class Requirement1(db.Model):
    pkey: int = db.Column(db.Integer, unique=False, nullable=False, primary_key=True)
    name: str = db.Column(db.Text, unique=False, nullable=False)
    inResidence: str = db.Column(db.Text, unique=False, nullable=False)
    fulfilledByMajor: str = db.Column(db.Text, unique=False, nullable=False)
    exampleClasses: str = db.Column(db.Text, unique=False, nullable=False)
    typeOfRequirement: str = db.Column(db.Text, unique=False, nullable=False)
    numHours: int = db.Column(db.Integer, unique=False, nullable=False)
    image: str = db.Column(db.Text, unique=False, nullable=False)
    web: str = db.Column(db.Text, unique=False, nullable=False)
    description: str = db.Column(db.Text, unique=False, nullable=False)

    def __init__(
        self,
        p,
        name="unknown",
        inResidence="unknown",
        fulfilledByMajor="",
        exampleClasses="unkown",
        typeOfRequirement="unknown",
        numHours=0,
        image="",
        web="",
        description="",
    ):
        self.pkey = p
        self.name = name
        self.inResidence = inResidence
        self.fulfilledByMajor = fulfilledByMajor
        self.exampleClasses = exampleClasses
        self.typeOfRequirement = typeOfRequirement
        self.numHours = numHours
        self.image = image
        self.web = web
        self.description = description

    def __repr__(self):
        return "<Requirement %r>" % self.name

