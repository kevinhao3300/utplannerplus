import main

main.db.create_all()

#image urls for diff Requirements
core_url = "https://i.imgur.com/UvOHTxT.jpg"
foreign_url = "https://i.imgur.com/Pap0lNz.jpg"
science_url = "https://i.imgur.com/7Lp1RE3.png"
major_url = "https://i.imgur.com/ELK27q6.png"

#create requirement classes to add to database
req1 = main.Requirements(
    identifier=1,
    name="Global Cultures",
    inResidence="yes",
    exampleClasses="WGS 340,UGS 303,C C 303,REE 302",
    typeOfRequirement="flag",
    numHours=3,
    image="https://i.imgur.com/PpU27wa.png",
    web="https://ugs.utexas.edu/flags/students/about/global-cultures",
)
req2 = main.Requirements(
    identifier=2,
    name="Ethics",
    inResidence="yes",
    exampleClasses="C S  349 CONTEMP ISSUES IN COMPUTER SCI , C S  378 BEHAVIORAL ETHICS: DGTL AGE",
    typeOfRequirement="flag",
    numHours=3,
    image="https://i.imgur.com/O7wATVT.png",
    web="https://ugs.utexas.edu/flags/students/about/ethics",
)
req3 = main.Requirements(
    identifier=3,
    name="Cultural Diversity",
    inResidence="yes",
    exampleClasses="MUS 307,AFR 301,J 334F,WGS 303",
    typeOfRequirement="flag",
    numHours=3,
    image="https://i.imgur.com/R3stk6sh.jpg",
    web="https://ugs.utexas.edu/flags/students/about/cultural-diversity",
)
req4 = main.Requirements(
    identifier=4,
    name="Quantitative Reasoning",
    inResidence="yes",
    exampleClasses="C S  302 COMPUTER FLUENCY, C S  314 DATA STRUCTURES, C S  331 ALGORITHMS AND COMPLEXITY",
    typeOfRequirement="flag",
    numHours=3,
    image="https://i.imgur.com/nEvN8yE.png",
    web="https://ugs.utexas.edu/flags/students/about/quantitative-reasoning",
)
req5 = main.Requirements(
    identifier=5,
    name="Independent Inquiry",
    inResidence="yes",
    exampleClasses="C S  439 PRINCIPLES OF COMPUTER SYS-C S, C S  354S GAME DEV CAPSTNE: 2-D GAMES, C S  371P OBJECT-ORIENTED PROGRAMMING",
    typeOfRequirement="flag",
    numHours=3,
    image="https://i.imgur.com/n0mnpvi.png",
    web="https://ugs.utexas.edu/flags/students/about/Independent-inquiry",
)
req6 = main.Requirements(
    identifier=6,
    name="Writing",
    inResidence="yes",
    exampleClasses="C S  349 CONTEMP ISSUES IN COMPUTER SCI, C S  373 SOFTWARE ENGINEERING",
    typeOfRequirement="flag",
    numHours=6,
    image="https://i.imgur.com/zXOZofw.png",
    web="https://ugs.utexas.edu/flags/students/about/writing",
)
req7 = main.Requirements(
    identifier=7,
    name="First Year Signature Course",
    inResidence="yes",
    exampleClasses="UGS 302, UGS 303",
    typeOfRequirement="core",
    numHours=3,
    image=core_url,
    web="https://ugs.utexas.edu/sig",
)
req8 = main.Requirements(
    identifier=8,
    name="English Composition",
    inResidence="yes",
    exampleClasses="RHE 306, RHE 309K",
    typeOfRequirement="core",
    numHours=3,
    image=core_url,
    web="https://catalog.utexas.edu/general-information/academic-policies-and-procedures/core-curriculum/",
)
req9 = main.Requirements(
    identifier=9,
    name="Humanities",
    inResidence="yes",
    exampleClasses="E 316L, E 316M, E 316N",
    typeOfRequirement="core",
    numHours=3,
    image=core_url,
    web="https://catalog.utexas.edu/general-information/academic-policies-and-procedures/core-curriculum/",
)
req10 = main.Requirements(
    identifier=10,
    name="American & Texas Government",
    inResidence="yes",
    exampleClasses="GOV 310L, GOV 312L",
    typeOfRequirement="core",
    numHours=6,
    image=core_url,
    web="https://catalog.utexas.edu/general-information/academic-policies-and-procedures/core-curriculum/",
)
req11 = main.Requirements(
    identifier=11,
    name="American History",
    inResidence="yes",
    exampleClasses="HIS 315K, HIS 315L",
    typeOfRequirement="core",
    numHours=6,
    image=core_url,
    web="https://catalog.utexas.edu/general-information/academic-policies-and-procedures/core-curriculum/",
)
req12 = main.Requirements(
    identifier=12,
    name="Social & Behavioral Science",
    inResidence="yes",
    exampleClasses="PSY 301",
    typeOfRequirement="core",
    numHours=3,
    image=core_url,
    web="https://catalog.utexas.edu/general-information/academic-policies-and-procedures/core-curriculum/",
)
req13 = main.Requirements(
    identifier=13,
    name="Visual & Performing Arts",
    inResidence="yes",
    exampleClasses="ARH 301",
    typeOfRequirement="core",
    numHours=3,
    image=core_url,
    web="https://art.utexas.edu/undergraduate/courses/vapa",
)
req14 = main.Requirements(
    identifier=14,
    name="Foreign Language",
    inResidence="yes",
    exampleClasses="CHI 604, CHI 606",
    typeOfRequirement="Foreign Language",
    numHours=6,
    image=foreign_url,
    web="https://liberalarts.utexas.edu/languages/",
)
req15 = main.Requirements(
    identifier=15,
    name="Science Sequence",
    inResidence="yes",
    exampleClasses="CH 301, CH 302, BIO 311C, BIO 311D",
    typeOfRequirement="Introductory Science",
    numHours=6,
    image=science_url,
    web="https://cns.utexas.edu/images/CNS/Deans_Office/Student_Records/Web.DegPlans/2018-20_checklists/BS_CS_OPTION1_1820.pdf",
)
req16 = main.Requirements(
    identifier=16,
    name="Additional Science",
    inResidence="yes",
    exampleClasses="GEO 303, M325K",
    typeOfRequirement="Introductory Science",
    numHours=3,
    image=science_url,
    web="https://cns.utexas.edu/images/CNS/Deans_Office/Student_Records/Web.DegPlans/2018-20_checklists/BS_CS_OPTION1_1820.pdf",
)
req17 = main.Requirements(
    identifier=17,
    name="Linear Algebra",
    inResidence="yes",
    exampleClasses="M 340L, SDS 329C, M 341",
    typeOfRequirement="Major",
    numHours=3,
    image=major_url,
    web="https://cns.utexas.edu/images/CNS/Deans_Office/Student_Records/Web.DegPlans/2018-20_checklists/BS_CS_OPTION1_1820.pdf",
)
req18 = main.Requirements(
    identifier=18,
    name="Probability",
    inResidence="yes",
    exampleClasses="SDS 321, M 362K",
    typeOfRequirement="Major",
    numHours=3,
    image=major_url,
    web="https://cns.utexas.edu/images/CNS/Deans_Office/Student_Records/Web.DegPlans/2018-20_checklists/BS_CS_OPTION1_1820.pdf",
)
req19 = main.Requirements(
    identifier=19,
    name="Programming",
    inResidence="yes",
    exampleClasses="C S  312 INTRODUCTION TO PROGRAMMING, C S  314 DATA STRUCTURES, C S  314H DATA STRUCTURES: HONORS",
    typeOfRequirement="Major",
    numHours=6,
    image=major_url,
    web="https://www.cs.utexas.edu/undergraduate-program/academics/curriculum",
)
req20 = main.Requirements(
    identifier=20,
    name="Theory",
    inResidence="yes",
    exampleClasses="C S  311 DISCRETE MATH FOR COMPUTER SCI, C S  311H DISCRETE MATH FOR COMPUTER SCI: HONORS, C S  331 ALGORITHMS AND COMPLEXITY, C S  331H ALGORITHMS AND COMPLEXITY: HONORS",
    typeOfRequirement="Major",
    numHours=6,
    image=major_url,
    web="https://www.cs.utexas.edu/undergraduate-program/academics/curriculum",
)
req21 = main.Requirements(
    identifier=21,
    name="Systems",
    inResidence="yes",
    exampleClasses="C S  429 COMP ORGANIZATN & ARCH-C S, C S  429H COMP ORGANIZATN & ARCH-C S: HONORS, C S  439 PRINCIPLES OF COMPUTER SYS-C S, C S  439H PRINS OF COMPUTER SYS: HONORS",
    typeOfRequirement="Major",
    numHours=8,
    image=major_url,
    web="https://www.cs.utexas.edu/undergraduate-program/academics/curriculum",
)
req22 = main.Requirements(
    identifier=22,
    name="Upper Divison CS",
    inResidence="yes",
    exampleClasses="C S  371P OBJECT-ORIENTED PROGRAMMING, C S  375 COMPILERS, C S  373 SOFTWARE ENGINEERING",
    typeOfRequirement="Major",
    numHours=24,
    image=major_url,
    web="https://www.cs.utexas.edu/undergraduate-program/academics/curriculum",
)
req23 = main.Requirements(
    identifier=23,
    name="Minimum 2.0 GPA",
    inResidence="N/A",
    exampleClasses="N/A",
    typeOfRequirement="GPA",
    numHours=0,
    image="https://i.imgur.com/ZhwJruJ.jpg",
    web="https://catalog.utexas.edu/undergraduate/natural-sciences/degrees-and-programs/bs-computer-science/",
)

#add to database
main.db.session.add(req1)
main.db.session.add(req2)
main.db.session.add(req3)
main.db.session.add(req4)
main.db.session.add(req5)
main.db.session.add(req6)
main.db.session.add(req7)
main.db.session.add(req8)
main.db.session.add(req9)
main.db.session.add(req10)
main.db.session.add(req11)
main.db.session.add(req12)
main.db.session.add(req13)
main.db.session.add(req14)
main.db.session.add(req15)
main.db.session.add(req16)
main.db.session.add(req17)
main.db.session.add(req18)
main.db.session.add(req19)
main.db.session.add(req20)
main.db.session.add(req21)
main.db.session.add(req22)
main.db.session.add(req23)

main.db.session.commit()
