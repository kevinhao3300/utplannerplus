from bs4 import BeautifulSoup
import requests
import numpy as np
import main

#use BeautifulSoup to scrape prof images in CS dept
response = requests.get('https://www.cs.utexas.edu/people')
soup = BeautifulSoup(response.text, 'html.parser')
body = soup.findAll("img")
names = []

main.db.create_all()

#loop through all prof images
for x in body:
    #gets url
    s = x["src"]
    #string manipulation to extract name
    try:
        index = s.index('_', 75)
    except ValueError:
        continue
    last_name = s[75:index]
    if "profilepic" in last_name:
        continue
    index = index+1
    try:
        endindex = s.index('.jpg', index)
    except ValueError:
        continue
    first_name = s[index:endindex]
    if "_" in first_name:
        checkindex = s.index('_', index)
        first_name = s[index:checkindex]
    
    #string concatenation to get final full name
    full_name = last_name + ", " + first_name
    full_name = full_name.replace("-", " ")
    full_name = full_name.upper()
    #filter name
    if "DSC" in full_name or "0" in full_name:
        continue
    #find name in Professor model and add image url field
    prof = main.Professor.query.filter_by(name=full_name).first()
    if prof is not None:
        prof.image = s

main.db.session.commit()
    
