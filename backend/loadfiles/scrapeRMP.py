import requests
from bs4 import BeautifulSoup
import models

tags = ["GIVES GOOD FEEDBACK", "RESPECTED", "LOTS OF HOMEWORK", "ACCESSIBLE OUTSIDE CLASS", 
"GET READY TO READ", "PARTICIPATION MATTERS", "SKIP CLASS? YOU WON'T PASS.", "INSPIRATIONAL", 
"GRADED BY FEW THINGS", "TEST HEAVY", "GROUP PROJECTS", "CLEAR GRADING CRITERIA", "HILARIOUS", 
"BEWARE OF POP QUIZZES", "AMAZING LECTURES", "LECTURE HEAVY", "CARING", "EXTRA CREDIT", 
"SO MANY PAPERS", "TOUGH GRADER"]

models.db.create_all()

profTID = models.Professor1.query.all()
count = 1
for p in profTID:
    if p.tID != -1:
        response = requests.get('https://www.ratemyprofessors.com/ShowRatings.jsp?tid=' + str(p.tID))
        soup = BeautifulSoup(response.text, 'html.parser')
        body = soup.findAll("div")
        lvldiff = body[36].text
        try:
            index = lvldiff.index('Level')
            lvldiff = lvldiff[0:index]
        except ValueError:
            lvldiff = 'N/A'
        # lvldiff = lvldiff[0:index]
        p.levelOfDifficulty = lvldiff
        # print(lvldiff)
        wta = body[34].text
        if '%' in wta:
            p.wouldTakeAgain = wta
        else:
            p.wouldTakeAgain = "N/A"
        t = body[40].text
        result = ""
        for tag in tags:
            if tag in t.upper():
                result += tag + "***"
        result = result[0:-3]
        if result == "":
            result = "N/A"
        p.tags = result
        # print(result)
    else:
        p.levelOfDifficulty = "N/A"
        p.wouldTakeAgain = "N/A"
        p.tags = "N/A"
    count+=1
    if count%100 == 0:
        print("commit" + str(count))
        # models.db.session.commit()

models.db.session.commit()

models.db.session.commit()
