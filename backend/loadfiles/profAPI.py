import numpy as np
import main

'''
this file reads from the Classes data in the database to create professor models and add them to the database.
Still need to add the data from the api to the new instances and format returns.
'''

main.db.create_all()
classes = main.db.session.query(main.Classes)
classes = classes.distinct(main.Classes.instructor).all()
identifier = 0
# # populate the class table in the database
for c in classes:
    instructors = c.instructor[2:-2].split("', '")
    for name in instructors:
        check = main.db.session.query(main.Professor)
        check = check.filter_by(name=name).first()
        if check is None:
            p = main.Professor(identifier)
            p.classes_taught = c.name
            p.name = name
            main.db.session.add(p)
        else:
            check.classes_taught = check.classes_taught + " / " + c.name
        identifier += 1

main.db.session.commit()
