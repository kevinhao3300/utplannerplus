from flask import Flask, jsonify, send_file, render_template, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_, func, String
from sqlalchemy.sql.expression import cast
from dataclasses import dataclass
from flask_cors import CORS
import cata
import time
import ast

# create app
app = Flask(
    __name__,
    static_folder="../frontend/build/static",
    template_folder="../frontend/build",
)
CORS(app)

# render return templates for request all, and request single
p1 = "postgresql://hookem:03JBURiI99jJqdpTPIrK@utplannerplus."
p2 = "cu1xjdswa4fl.us-east-2.rds.amazonaws.com/postgres"
s = p1 + p2

# set  up database
app.config["SQLALCHEMY_DATABASE_URI"] = s
db = SQLAlchemy(app)
# import after creation of db because of dependency
import Requirements, Classes, Professors


"""
ROUTES
"""


@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def index(path):
    return render_template("index.html")


def parse_args(NAME_TO_ATTR, SEARCHABLE_ATTRS):
    # grab a single instance
    page_number = request.args.get("page_number")
    page_size = request.args.get("page_size")
    exact = False
    if not page_number or not page_size:
        page_number = 1
        page_size = 1
        exact = True
    else:
        page_number = int(page_number)
        page_size = int(page_size)

    # arg to sort by
    sort_by = request.args.get("sort_by")
    # 'True' for descending sort
    reverse = request.args.get("reverse")

    # default to sort by pkey
    if sort_by == "":
        sort_arg = NAME_TO_ATTR["pkey"]
    else:
        sort_arg = (
            NAME_TO_ATTR[sort_by].desc() if reverse == "True" 
            else NAME_TO_ATTR[sort_by]
        )

    filter_by = request.args.get("filter_by")
    filter_string = request.args.get("filter_string")
    # default to get all model instances
    if filter_by == "":
        filter_arg = True
    else:
        filter_string = filter_string.lower()
        # look through all attributes
        if filter_by == "all":
            filter_arg = or_(
                func.lower(cast(attr, String)).contains(filter_string) 
                for attr in SEARCHABLE_ATTRS
            )
        else:
            c = cast(NAME_TO_ATTR[filter_by], String)
            # exact match search
            if exact:
                filter_arg = (
                    func.lower(c) == filter_string
                )
            # containment search
            else:
                filter_arg = func.lower(c).contains(filter_string)

    return filter_arg, sort_arg, page_number, page_size


def make_db_query(model, filter_arg, sort_arg, page_number, page_size):

    # grab data based on query paramaters
    data = (
        db.session.query(model)
        .filter(filter_arg)
        .order_by(sort_arg)
        .paginate(page_number, page_size, False)
        .items
    )

    # count total number of instances satisfying the query parameters
    count = (
        db.session.query(model)
            .filter(filter_arg)
            .order_by(sort_arg)
            .count()
    )
    # construct response
    response = {"data": data, "count": count}

    return response


@app.route("/api/Courses/")
def classes_route():

    NAME_TO_ATTR_CLASS = {
        "name": Classes.Class1.name,
        "number": Classes.Class1.number,
        "level": Classes.Class1.level,
        "semester": Classes.Class1.semester,
        "department": Classes.Class1.department,
        "unique": Classes.Class1.unique,
        "instructor": Classes.Class1.instructor,
        "req": Classes.Class1.req,
        "pkey": Classes.Class1.pkey,
    }

    SEARCHABLE_ATTRS = [
        Classes.Class1.name,
        Classes.Class1.number,
        Classes.Class1.level,
        Classes.Class1.semester,
        Classes.Class1.department,
        Classes.Class1.unique,
        Classes.Class1.instructor,
        Classes.Class1.req,
    ]

    response = make_db_query(
        Classes.Class1, *parse_args(NAME_TO_ATTR_CLASS, SEARCHABLE_ATTRS)
    )

    # fix lists from strings to lists
    for i in range(len(response["data"])):
        response["data"][i].days = ast.literal_eval(
            str(response["data"][i].days))
        response["data"][i].description = ast.literal_eval(
            str(response["data"][i].description))
        response["data"][i].hour = ast.literal_eval(
            str(response["data"][i].hour))
        response["data"][i].instructionMode = ast.literal_eval(
            str(response["data"][i].instructionMode))
        response["data"][i].room = ast.literal_eval(
            str(response["data"][i].room))
        response["data"][i].instructor = ast.literal_eval(
            str(response["data"][i].instructor))

    return jsonify(response)


@app.route("/api/Professors/")
def professors_route():
    NAME_TO_ATTR_PROF = {
        "name": Professors.Professor1.name,
        "ratingClass": Professors.Professor1.ratingClass,
        "levelOfDifficulty": Professors.Professor1.levelOfDifficulty,
        "numRatings": Professors.Professor1.numRatings,
        "overallRating": Professors.Professor1.overallRating,
        "department": Professors.Professor1.department,
        "pkey": Professors.Professor1.pkey,
    }

    SEARCHABLE_ATTRS = [
        Professors.Professor1.name,
        Professors.Professor1.ratingClass,
        Professors.Professor1.levelOfDifficulty,
        Professors.Professor1.numRatings,
        Professors.Professor1.overallRating,
        Professors.Professor1.department,
    ]

    response = make_db_query(
        Professors.Professor1, *parse_args(NAME_TO_ATTR_PROF, SEARCHABLE_ATTRS)
    )

    return jsonify(response)


@app.route("/api/Requirements/")
def requirements_route():

    NAME_TO_ATTR_REQUIREMENT = {
        "name": Requirements.Requirement1.name,
        "inResidence": Requirements.Requirement1.inResidence,
        "fulfilledByMajor": Requirements.Requirement1.fulfilledByMajor,
        "typeOfRequirement": Requirements.Requirement1.typeOfRequirement,
        "numHours": Requirements.Requirement1.numHours,
        "exampleClasses": Requirements.Requirement1.exampleClasses,
        "pkey": Requirements.Requirement1.pkey,
    }

    SEARCHABLE_ATTRS = [
        Requirements.Requirement1.name,
        Requirements.Requirement1.inResidence,
        Requirements.Requirement1.fulfilledByMajor,
        Requirements.Requirement1.typeOfRequirement,
        Requirements.Requirement1.numHours,
        Requirements.Requirement1.exampleClasses,
    ]

    response = make_db_query(
        Requirements.Requirement1,
        *parse_args(NAME_TO_ATTR_REQUIREMENT, SEARCHABLE_ATTRS)
    )

    return jsonify(response)


# run the app
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, threaded=True, debug=True)
