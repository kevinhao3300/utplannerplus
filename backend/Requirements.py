import main
from dataclasses import dataclass
from main import db

# REQUIREMENT MODEL


@dataclass
class Requirement1(db.Model):
    pkey: int = db.Column(db.Integer, unique=False,
                          nullable=False, primary_key=True)
    name: str = db.Column(db.Text, unique=False, nullable=False)
    inResidence: str = db.Column(db.Text, unique=False, nullable=False)
    fulfilledByMajor: str = db.Column(db.Text, unique=False, nullable=False)
    exampleClasses: str = db.Column(db.Text, unique=False, nullable=False)
    typeOfRequirement: str = db.Column(db.Text, unique=False, nullable=False)
    numHours: int = db.Column(db.Integer, unique=False, nullable=False)
    image: str = db.Column(db.Text, unique=False, nullable=False)
    web: str = db.Column(db.Text, unique=False, nullable=False)
    description: str = db.Column(db.Text, unique=False, nullable=False)

    def __init__(
        self,
        p,
        name="unknown",
        inResidence="unknown",
        fulfilledByMajor="",
        exampleClasses="unkown",
        typeOfRequirement="unknown",
        numHours=0,
        image="",
        web="",
        description="",
    ):
        self.pkey = p
        self.name = name
        self.inResidence = inResidence
        self.fulfilledByMajor = fulfilledByMajor
        self.exampleClasses = exampleClasses
        self.typeOfRequirement = typeOfRequirement
        self.numHours = numHours
        self.image = image
        self.web = web
        self.description = description

    def __repr__(self):
        return "<Requirement %r>" % self.name
