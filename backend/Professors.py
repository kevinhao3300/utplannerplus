import main
from dataclasses import dataclass
from main import db

# PROF MODEL


@dataclass
class Professor1(db.Model):
    pkey: int = db.Column(db.Integer, unique=False,
                          nullable=False, primary_key=True)
    name: str = db.Column(db.Text, unique=False, nullable=False)
    tID: int = db.Column(db.Integer, unique=False, nullable=False)
    department: str = db.Column(db.Text, unique=False, nullable=False)
    ratingClass: str = db.Column(db.Text, unique=False, nullable=False)
    overallRating: str = db.Column(db.Text, unique=False, nullable=False)
    numRatings: str = db.Column(db.Text, unique=False, nullable=False)
    image: str = db.Column(db.Text, unique=False, nullable=False)
    wouldTakeAgain: str = db.Column(db.Text, unique=False, nullable=False)
    levelOfDifficulty: str = db.Column(db.Text, unique=False, nullable=False)
    tags: str = db.Column(db.Text, unique=False, nullable=False)

    def __init__(
        self,
        p,
        name="unknown",
        tid=-1,
        tDept="unknown",
        ratingClass="unknown",
        overallRating="0",
        numRatings="N/A",
        image="",
        wouldTakeAgain="N/A",
        levelOfDifficulty="N/A",
        tags="",
    ):
        self.id = p
        self.name = name
        self.tID = tid
        self.department = tDept
        self.ratingClass = ratingClass
        self.overallRating = overallRating
        self.numRatings = numRatings
        self.image = image
        self.wouldTakeAgain = wouldTakeAgain
        self.levelOfDifficulty = levelOfDifficulty
        self.tags = tags
