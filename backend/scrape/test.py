import requests
from bs4 import BeautifulSoup, Tag, NavigableString
import json
import numpy as np
import sys
import time
import os


def safe_get(url, headers, params, cookies):
    response = None
    while response == None:
        try:
            response = requests.get(
                url, headers=headers, params=params, cookies=cookies)
            break
        except requests.exceptions.ConnectionError:
            time.sleep(5)
            continue
    return response


def get_info(child, cur_class_name, level, semester, department):
    class_instance = {'Name': cur_class_name, 'Level': level,
                      'Semester': semester, 'Department': department}
    for grand_child in child.children:
        if isinstance(grand_child, NavigableString):
            continue

        key = grand_child['data-th']
        value = []

        for information in grand_child.children:
            if isinstance(information, NavigableString) or information.name == 'br':
                continue
            if information.name == 'a':
                response = safe_get(f"https://qual.its.utexas.edu{information['href']}",
                                    headers=headers, params=params, cookies=cookies)
                soup = BeautifulSoup(response.text, 'html.parser')
                section = soup.find('section', {'id': 'details'})
                if section:
                    class_instance['Description'] = [
                        p_tag.text for p_tag in section.findAll('p')]
                else:
                    class_instance['Description'] = []

            val = information.get_text().strip()
            if val:
                value.append(val)
        class_instance[key] = value

    # for blank instruction modes
    if not class_instance['Instruction Mode']:
        class_instance['Instruction Mode'].append('Face-to-face')

    # print(class_instance)
    return class_instance


def parse_response(response, level, semester, department):
    soup = BeautifulSoup(response.text, 'html.parser')
    if not soup.findAll("tbody"):
        return 'error'
    body = soup.findAll("tbody")[0]
    cur_class_name = None
    all_classes = []
    for child in body:
        if child.name == None:
            continue
        # gray row case
        if child.has_attr('class'):
            all_classes.append(
                get_info(child, cur_class_name, level, semester, department))
        else:
            if child.name != 'tr':
                continue
            # differentiate white row and class name
            grand_child_iter = iter(child.children)
            first_child = next(grand_child_iter)
            first_child = next(grand_child_iter)
            # class name case
            if first_child.has_attr('class'):
                cur_class_name = first_child.find('h2').get_text().strip()
            # white row case
            else:
                all_classes.append(
                    get_info(child, cur_class_name, level, semester, department))

    return all_classes


if __name__ == '__main__':
    cookies = {
        'SC': 'AQEBBwID6gIQODZGODFFQTgzOThGMURBNwYkUkRWTys3ZFhZSVM1dm43bDF4anRZUVBHdjlTZXZIK0tjL2c1BAoxNjAzOTMxMjg0BQ43Mi4xODAuMTExLjEwMAMHa2poMjg1OAoBWQiAlEc7vC1Y5vBrJ1rmcF6mdKCJoqWsUPN7GGqrHLB+lFzXBzk2ZsAmcgRgoATTnnwHBCqxsceUHL1KyFHftFlf3Wa1tIOqZDALUZXMjV8Ztdj41fc/k6nP8SEVlvvG9UH6vBmjkGIa9KL9SqPITNtZjExLW33WptB5w7pGkBEvIwA=',
        'ut_persist': '2332469440.47873.0000',
        '_shibsession_64656661756c7468747470733a2f2f7175616c2e6974732e7574657861732e6564752f73686962626f6c657468': '_31b442ce872dbe1274d3fe21c9151dbc',
    }

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'Cache-Control': 'max-age=0',
    }
    years = [str(year) for year in range(2008, 2021)]
    semesters = ['2', '6', '9']
    levels = ['L', 'U', 'G']
    departments = ['GEO', 'GER', 'GRC', 'GOV', 'GRS', 'GK', 'GUI', 'HAR', 'HED',
                   'HEB', 'HIN', 'HIS', 'HDF', 'H E', 'HMN', 'INF', 'I B', 'ISL', 'ITL', 'ITC', 'JPN', 'J S', 'J', 'KIN',
                   'KOR']

    num_sections = 5
    i = int(sys.argv[1])
    val = len(departments) // num_sections
    section = departments[i*val:(i+1)*val]

    names = set(os.listdir('all_departments'))

    for department in section:
        if f"{department.replace(' ', '_')}_classes.npy" in names:
            continue
        all_classes = []
        for year in years:
            for semester in semesters:
                for level in levels:
                    last_id = None
                    while True:
                        params = (
                            ('ccyys', year + semester),
                            ('search_type_main', 'FIELD'),
                            ('fos_fl', department),
                            ('level', level),
                            ('next_unique', last_id),
                        )
                        response = safe_get(f'https://qual.its.utexas.edu/apps/registrar/course_schedule/{year+semester}/results/',
                                            headers=headers, params=params, cookies=cookies)

                        result = parse_response(
                            response, level, year + semester, department)
                        if result == 'error':
                            break
                        all_classes.extend(result)
                        last_id = str(
                            int(all_classes[-1]['Unique'][0]) + 5)

        np.save(f"all_departments/{department.replace(' ', '_')}_classes",
                all_classes, allow_pickle=True)
        print('done with ' + department)
