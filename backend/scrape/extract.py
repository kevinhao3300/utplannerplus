import numpy as np
import os

departments = ['ACC', 'ACF', 'ADV', 'ASE', 'AFR', 'AFS', 'ASL', 'AMS',
    'AHC', 'ANT', 'ALD', 'ARA', 'ARY', 'ARE', 'ARI', 'ARC', 'AED', 'ARH', 
    'AAS', 'ANS', 'AST', 'BSN', 'BEN', 'BCH', 'BIO', 'BME', 'BDP', 'B A',
    'CHE', 'CH', 'CHI', 'C E', 'CLA', 'C C', 'CGS', 'CSD', 'COM', 'CMS',
    'CRP', 'C L', 'CAM', 'C S', 'CON', 'CLS', 'EDC', 'CZ', 'DAN', 'DES',
    'DEV', 'D B', 'DRS', 'DCH', 'ECO', 'EDA', 'EDP', 'E E', 'EER', 'ENM',
    'E M', 'E', 'ENS', 'EUP', 'EUS', 'FIN', 'F A', 'F S', 'FLU', 'FLE',
    'FR', 'F C', 'F H', 'G E', 'GRG', 'GEO', 'GER', 'GRC', 'GOV', 'GRS',
    'GK', 'GUI', 'HAR', 'HED', 'HEB', 'HIN', 'HIS', 'HDF', 'H E', 'HMN',
    'INF', 'I B', 'ISL', 'ITL', 'ITC', 'JPN', 'J S', 'J', 'KIN',
    'KOR', 'LAR', 'LAT', 'LAS', 'LAW', 'LEB', 'L A', 'LAH',
    'LIN', 'MAL', 'MAN', 'MIS', 'MFG', 'MNS', 'MKT', 'MSE',
    'MST', 'M', 'M E', 'MDV', 'MAS', 'MES', 'M S', 'MOL', 'MUS', 'NSC', 
    'N S', 'NEU', 'NOR', 'N', 'NTR', 'OBO', 'OPR', 'O M', 'ORI', 'ORG', 
    'PER', 'PRS', 'PGE', 'PHR', 'PHL', 'PED', 'P S', 'PHY', 'PIA', 'POL', 
    'POR', 'PRC', 'PSY', 'P A', 'P R', 'RTF', 'R E', 'R S', 'RHE', 'R M', 
    'REE', 'RUS', 'SAN', 'SAX', 'SCA', 'STC', 'SCI', 'SME', 'STS', 'S C', 
    'SLA', 'S S', 'S W', 'SOC', 'SPN', 'SED', 'STA', 'ART', 'SWE', 'TAM', 
    'TEL', 'TXA', 'T D', 'TRO', 'TRU', 'TBA', 'TUR', 'T C', 'UGS', 'URB', 
    'URD', 'UTL', 'UTS', 'VTN', 'VIA', 'VIO', 'V C', 'VAS', 'VOI', 'WGS', 
    'WRT', 'YID', 'YOR']

for department in departments:
    cur_name = f"{department.replace(' ', '_')}_classes.npy"
    classes = np.load(
        f'all_departments/{cur_name}', allow_pickle=True)
    print(cur_name, classes.shape)
