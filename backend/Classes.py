import main
from dataclasses import dataclass
from main import db

# MOST RECENT DISTINCT CLASSES


@dataclass
class distinctClass(db.Model):
    pkey: int = db.Column(db.Integer, unique=False,
                          nullable=False, primary_key=True)
    name: str = db.Column(db.Text, unique=False, nullable=False)
    level: str = db.Column(db.Text, unique=False, nullable=False)
    semester: str = db.Column(db.Text, unique=False, nullable=False)
    department: str = db.Column(db.Text, unique=False, nullable=False)
    unique: int = db.Column(db.Integer, unique=False, nullable=False)
    days: str = db.Column(db.Text, unique=False, nullable=False)
    hour: str = db.Column(db.Text, unique=False, nullable=False)
    room: str = db.Column(db.Text, unique=False, nullable=False)
    instructionMode: str = db.Column(db.Text, unique=False, nullable=False)
    instructor: str = db.Column(db.Text, unique=False, nullable=False)
    status: str = db.Column(db.Text, unique=False, nullable=False)
    req: str = db.Column(db.Text, unique=False, nullable=False)
    description: str = db.Column(db.Text, unique=False, nullable=False)

    def __init__(
        self,
        identifier,
        name,
        level,
        semester,
        department,
        unique,
        days,
        hour,
        room,
        instructionMode,
        instructor,
        status,
        req="None",
        description="",
    ):
        self.identifier = identifier
        self.name = name
        self.level = level
        self.semester = semester
        self.department = department
        self.unique = unique
        self.days = days
        self.hour = hour
        self.room = room
        self.instructionMode = instructionMode
        self.instructor = instructor
        self.status = status
        self.req = req
        self.description = description

    def __repr__(self):
        return "<Class %r>" % self.name


@dataclass
class Class1(db.Model):
    pkey: int = db.Column(db.Integer, unique=False,
                          nullable=False, primary_key=True)
    name: str = db.Column(db.Text, unique=False, nullable=False)
    number: str = db.Column(db.Text, unique=False, nullable=False)
    level: str = db.Column(db.Text, unique=False, nullable=False)
    semester: str = db.Column(db.Text, unique=False, nullable=False)
    department: str = db.Column(db.Text, unique=False, nullable=False)
    unique: int = db.Column(db.Integer, unique=False, nullable=False)
    days: str = db.Column(db.Text, unique=False, nullable=False)
    hour: str = db.Column(db.Text, unique=False, nullable=False)
    room: str = db.Column(db.Text, unique=False, nullable=False)
    req: str = db.Column(db.Text, unique=False, nullable=False)
    instructionMode: str = db.Column(db.Text, unique=False, nullable=False)
    instructor: str = db.Column(db.Text, unique=False, nullable=False)
    description: str = db.Column(db.Text, unique=False, nullable=False)

    def __init__(
        self,
        p,
        name,
        number,
        level,
        semester,
        department,
        unique,
        days,
        hour,
        room,
        req,
        instructionMode,
        instructor,
        description="",
    ):
        self.pkey = p
        self.name = name
        self.number = number
        self.level = level
        self.semester = semester
        self.department = department
        self.unique = unique
        self.days = days
        self.hour = hour
        self.room = room
        self.req = req
        self.instructionMode = instructionMode
        self.instructor = instructor
        self.description = description

    def __repr__(self):
        return "<Class %r>" % self.name
