import sqlite3


def get_grades(conn, prof_name, course_dept, course_num):
    """
    Obtain grade distribution given a certain course and professor
    Args:
        conn (database connection): used to connect to database
        course_dept (string): course department
        prof_name (string): prof name formatted firstname, lastname
        course_num (string): course num
    Returns:
        result (object): grade distribution
    """

    # make database query
    cur = conn.cursor()
    query = "SELECT * from agg WHERE sem like '%Aggregate%'"
    query += " and dept like '%" + course_dept + "%'"
    query += " and prof like '%" + prof_name + "%'"
    query += " and course_nbr like '%" + course_num + "%'"
    cur.execute(query)

    # fetch row, return None if nothing fetched
    rows = cur.fetchall()
    if len(rows) == 0:
        return None

    A = A_minus = B_plus = B = B_minus = C_plus = C = C_minus = D_plus = D = D_minus = F = 0
    # count number of each type of grade
    for row in rows:
        A += row[6]
        A_minus += row[7]
        B_plus += row[8]
        B += row[9]
        B_minus += row[10]
        C_plus += row[11]
        C += row[12]
        C_minus += row[13]
        D_plus += row[14]
        D += row[15]
        D_minus += row[16]
        F += row[17]

    result = {
        'A': A,
        'A-': A_minus,
        'B+': B_plus,
        'B': B,
        'B-': B_minus,
        'C+': C_plus,
        'C': C,
        'C-': C_minus,
        'D+': D_plus,
        'D': D,
        'D-': D_minus,
        'F': F
    }
    return result


def create_connection(db_file):
    """
    Given a database file, establish a connection
    Args:
        db_file (file): database file
    Returns:
        conn (database connection): used to connect to database
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn


if __name__ == '__main__':
    # course_dept = request.get_json()['course_dept']
    # course_num = request.get_json()['course_num']
    # prof_first = request.get_json()['prof_first']
    # prof_last = request.get_json()['prof_last']


    course_dept = 'C S'
    course_num = '314'
    prof_first = 'Michael'
    prof_last = 'Scott'


    # reformat prof name
    prof_first = prof_first.replace('\'', '')
    prof_last = prof_last.replace('\'', ' ')
    prof_last = prof_last.split()
    prof_name = prof_last[len(prof_last) - 1] + ", " + prof_first

    # code to fetch database from remote
    # receive = requests.get('https://rawgit.com/shishirjessu/db/master/grades.db')
    # with open(r'grades.db', 'wb') as f:
    #     f.write(receive.content)

    # connect to database file
    database = r'grades.db'
    conn = create_connection(database)
    with conn:
        # obtain grade distribution
        grades = get_grades(conn, prof_name, course_dept, course_num)

    print(grades)
