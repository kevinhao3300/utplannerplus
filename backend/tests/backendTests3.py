import requests
import unittest
import random
import re

# unit tests for the python code using unittest


class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        pass

    # These are the tests that check the database for our classes to see if we have the
    # information of each class correctly. It tests if we have a professor, flags, a class, whether the
    # classes are upper, lower, or graduate division
    def test_10(self):
        # checks a specific class
        params = {
            'filter_by': 'department',
            'filter_string': 'BIO',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        # These check the data types of our elements
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        # Checks that they are upper, lower, or graduate division
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        # Checks if we are searching the right class
        assert(r['data'][0]["name"] == "ADVANCED STUDY AND RESEARCH")
        # If we have the correct semester
        assert(r['data'][0]["semester"] == "20182")
        # Checks if it has the correct department
        assert(r['data'][0]["department"] == "BIO")
        assert(r['data'][0]["unique"] > 0)
        # Checks if we have the correct unique id's
        assert(r['data'][0]["unique"] == 49560)

    def test_11(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'PSY',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "INTERNSHIP IN CLINICAL PSY")
        assert(r['data'][0]["semester"] == "20162")
        assert(r['data'][0]["department"] == "PSY")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 42475)

    def test_12(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'C S',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "COMPUTER FLUENCY")
        assert(r['data'][0]["semester"] == "20162")
        assert(r['data'][0]["department"] == "C S")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 50830)

    def test_13(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'MNS',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "RESEARCH IN MARINE SCIENCE")
        assert(r['data'][0]["semester"] == "20172")
        assert(r['data'][0]["department"] == "MNS")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 53815)

    def test_14(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'DES',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "GRADUATE INTERNSHIP IN DESIGN")
        assert(r['data'][0]["semester"] == "20172")
        assert(r['data'][0]["department"] == "DES")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 20513)

    def test_15(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'GOV',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "THESIS")
        assert(r['data'][0]["semester"] == "20162")
        assert(r['data'][0]["department"] == "GOV")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 38225)

    def test_16(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'ACC',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "FOUNDATIONS OF ACCOUNTING")
        assert(r['data'][0]["semester"] == "20162")
        assert(r['data'][0]["department"] == "ACC")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 2340)

    def test_17(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'CHI',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "ACCELERATED FIRST-YEAR CHINESE")
        assert(r['data'][0]["semester"] == "20162")
        assert(r['data'][0]["department"] == "CHI")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 31110)

    def test_18(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'BME',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "INTRODUCTION TO COMPUTING")
        assert(r['data'][0]["semester"] == "20162")
        assert(r['data'][0]["department"] == "BME")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 14005)

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main()
