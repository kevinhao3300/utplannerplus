import requests
import unittest
import random
import re

# unit tests for the python code using unittest


class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        pass

    # These are the tests that check the database for our classes to see if we have the
    # information of each class correctly. It tests if we have a professor, flags, a class, whether the
    # classes are upper, lower, or graduate division
    def test_30(self):
        # checks a specific class
        params = {
            'filter_by': 'department',
            'filter_string': 'BIO',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        # These check the data types of our elements
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        # Checks that they are upper, lower, or graduate division
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        # Checks if we are searching the right class
        assert(r['data'][0]["name"] == "ADVANCED STUDY AND RESEARCH")
        # If we have the correct semester
        assert(r['data'][0]["semester"] == "20182")
        # Checks if it has the correct department
        assert(r['data'][0]["department"] == "BIO")
        assert(r['data'][0]["unique"] > 0)
        # Checks if we have the correct unique id's
        assert(r['data'][0]["unique"] == 49560)

    def test_31(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'HIS',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "MODERN WORLD")
        assert(r['data'][0]["semester"] == "20162")
        assert(r['data'][0]["department"] == "HIS")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 38385)

    def test_32(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'E E',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "SPEC PROBS IN ELEC & COMP ENGR")
        assert(r['data'][0]["semester"] == "20169")
        assert(r['data'][0]["department"] == "E E")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 16680)

    def test_33(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'LAW',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "CLINIC, ADVANCED")
        assert(r['data'][0]["semester"] == "20182")
        assert(r['data'][0]["department"] == "LAW")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 29200)

    def test_34(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'C S',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "COMPUTER FLUENCY")
        assert(r['data'][0]["semester"] == "20162")
        assert(r['data'][0]["department"] == "C S")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 50830)

    def test_35(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'ADV',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "DISSERTATION  (Whole session)")
        assert(r['data'][0]["semester"] == "20166")
        assert(r['data'][0]["department"] == "ADV")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 72690)

    def test_36(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'EUS',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "INTRO TO EUROPEAN STUDIES")
        assert(r['data'][0]["semester"] == "20162")
        assert(r['data'][0]["department"] == "EUS")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 35435)

    def test_37(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'RUS',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "DISSERTATION  (Whole session)")
        assert(r['data'][0]["semester"] == "20176")
        assert(r['data'][0]["department"] == "RUS")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 85240)

    def test_38(self):
        params = {
            'filter_by': 'department',
            'filter_string': 'BME',
            'sort_by': ''
        }
        r = requests.get('http://0.0.0.0:80/api/Courses/',
                         params=params).json()
        assert isinstance(r['data'][0]["instructor"], list)
        assert isinstance(r['data'][0]["days"], list)
        assert isinstance(r['data'][0]["level"], str)
        assert isinstance(r['data'][0]["name"], str)
        assert isinstance(r['data'][0]["semester"], str)
        assert isinstance(r['data'][0]["unique"], int)
        assert isinstance(r['data'][0]["hour"], list)
        assert isinstance(r['data'][0]["instructionMode"], list)
        assert(len(r['data'][0]["level"]) > 0)
        assert(r['data'][0]["level"] == 'L' or r['data'][0]
               ["level"] == 'U' or r['data'][0]["level"] == 'G')
        assert(len(r['data'][0]["name"]) > 0)
        assert(r['data'][0]["name"] == "INTRODUCTION TO COMPUTING")
        assert(r['data'][0]["semester"] == "20162")
        assert(r['data'][0]["department"] == "BME")
        assert(r['data'][0]["unique"] > 0)
        assert(r['data'][0]["unique"] == 14005)

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main()
