import requests
import unittest
import random
import re

#unit tests for the python code using unittest

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        pass
    
    #There are tests that check to see if we are getting the information of the professors correctly
    #It tests that each teacher exists in our database, the ratings are floats,
    #if each professor has taught a class. Checking things on random.
    def test_0(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'126',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        #checks the name of this teacher
        assert r['data'][0]["name"] == "BRILEY, MARGARET E"  
        #a unique number given to each professor                  
        assert isinstance(r['data'][0]["pkey"], int)                            
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 126
        #checks if the ratings from ratemyprofessor is working
        assert isinstance(r['data'][0]["numRatings"], str)                     
        assert r['data'][0]["numRatings"] == "N/A"
        #checks if the ratings from ratemyprofessor is working
        assert isinstance(r['data'][0]["overallRating"], str)                  
        assert r['data'][0]["overallRating"] == "N/A"

    def test_1(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'700',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "CLEMENT, MICHAEL B"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 700
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"

    def test_2(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'6711',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "TRYBULA, AMANDA"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 6711
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"

    def test_3(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'1234',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "YANKEELOV, THOMAS"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 1234
        assert isinstance(r['data'][0]["numRatings"], str)
        assert float(r['data'][0]["numRatings"]) == 1.0
        assert isinstance(r['data'][0]["overallRating"], str)
        assert float(r['data'][0]["overallRating"]) == 5.0

    def test_4(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'4321',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "HAUSER, ELLIOTT"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 4321
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"

    def test_5(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'5',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "BARBE, ADAM"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 5
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"

    def test_6(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'784',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "LIU, DANIEL K"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 784
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"

    def test_7(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'333',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "EIBENSTEIN-ALVISI, I"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 333
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"

    def test_8(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'1',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "SHANK, GERALD C"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 1
        assert isinstance(r['data'][0]["numRatings"], str)
        assert float(r['data'][0]["numRatings"]) == 3.0
        assert isinstance(r['data'][0]["overallRating"], str)
        assert float(r['data'][0]["overallRating"]) == 4.7

    def tearDown(self):
        pass

if __name__ == "__main__":
    unittest.main()
