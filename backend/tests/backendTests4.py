import requests
import unittest
import random
import re

#unit tests for the python code using unittest

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        pass
    
    #There are tests that check to see if we are getting the information of the professors correctly
    #It tests that each teacher exists in our database, the ratings are floats,
    #if each professor has taught a class. Checking things on random.
    def test_20(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'126',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        #checks the name of this teacher
        assert r['data'][0]["name"] == "BRILEY, MARGARET E"  
        #a unique number given to each professor                  
        assert isinstance(r['data'][0]["pkey"], int)                            
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 126
        #checks if the ratings from ratemyprofessor is working
        assert isinstance(r['data'][0]["numRatings"], str)                     
        assert r['data'][0]["numRatings"] == "N/A"
        #checks if the ratings from ratemyprofessor is working
        assert isinstance(r['data'][0]["overallRating"], str)                  
        assert r['data'][0]["overallRating"] == "N/A"

    def test_21(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'311',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "HEGELICH, BJORN"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 311
        assert isinstance(r['data'][0]["numRatings"], str)
        assert float(r['data'][0]["numRatings"]) == 10.0
        assert isinstance(r['data'][0]["overallRating"], str)
        assert float(r['data'][0]["overallRating"]) == 1.5

    def test_22(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'678',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "WONG, RICHARD"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 678
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"

    def test_23(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'777',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "WARREN-CLEM, KEEGAN D"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 777
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"

    def test_24(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'999',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "INGRAM, MITCHELL D"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 999
        assert isinstance(r['data'][0]["numRatings"], str)
        assert float(r['data'][0]["numRatings"]) == 0.0
        assert isinstance(r['data'][0]["overallRating"], str)
        assert float(r['data'][0]["overallRating"]) == 0.0

    def test_25(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'3450',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "DAVIDSON, MARK A"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 3450
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"

    def test_26(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'2',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "MCCLEAREN, JENNIFER"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 2
        assert isinstance(r['data'][0]["numRatings"], str)
        assert float(r['data'][0]["numRatings"]) == 19.0
        assert isinstance(r['data'][0]["overallRating"], str)
        assert float(r['data'][0]["overallRating"]) == 4.2

    def test_27(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'1010',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "KNUTH, ERIC"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 1010
        assert isinstance(r['data'][0]["numRatings"], str)
        assert float(r['data'][0]["numRatings"]) == 0.0
        assert isinstance(r['data'][0]["overallRating"], str)
        assert float(r['data'][0]["overallRating"]) == 0.0

    def test_28(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'1',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "SHANK, GERALD C"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 1
        assert isinstance(r['data'][0]["numRatings"], str)
        assert float(r['data'][0]["numRatings"]) == 3.0
        assert isinstance(r['data'][0]["overallRating"], str)
        assert float(r['data'][0]["overallRating"]) == 4.7

    def tearDown(self):
        pass

if __name__ == "__main__":
    unittest.main()
