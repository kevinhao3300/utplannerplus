import requests
import unittest
import random

#unit tests for the python code using unittest

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        pass
    
    #Each test checks that we are getting the information of the professors correctly
    #It tests that each teacher exists in our database, the ratings are strings,
    #if each professor has taught a class
    def test_0(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'126',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        #checks the name of this teacher
        assert r['data'][0]["name"] == "BRILEY, MARGARET E"  
        #a unique number given to each professor                  
        assert isinstance(r['data'][0]["pkey"], int)                            
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 126
        #checks if the ratings from ratemyprofessor is working
        assert isinstance(r['data'][0]["numRatings"], str)                     
        assert r['data'][0]["numRatings"] == "N/A"
        #checks if the ratings from ratemyprofessor is working
        assert isinstance(r['data'][0]["overallRating"], str)                  
        assert r['data'][0]["overallRating"] == "N/A"

    def test_1(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'13',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "DANGEL, ULRICH C"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 13
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"

    def test_2(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'12',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "COKER, COLEMAN"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 12
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"


    def test_3(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'14',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "DANZE, ELIZABETH A"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 14
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"

    def test_4(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'15',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "DI PIAZZA, CHARLES H"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 15
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"

    def test_5(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'16',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "DUDLEY, TARA A"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 16
        assert isinstance(r['data'][0]["numRatings"], str)
        assert r['data'][0]["numRatings"] == "N/A"
        assert isinstance(r['data'][0]["overallRating"], str)
        assert r['data'][0]["overallRating"] == "N/A"

    def test_6(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'257',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "FRITZ, JENNIFER H"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 257
        assert isinstance(r['data'][0]["numRatings"], str)
        assert float(r['data'][0]["numRatings"]) == 154.0
        assert isinstance(r['data'][0]["overallRating"], str)
        assert float(r['data'][0]["overallRating"]) == 3.0

    def test_7(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'221',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "ELBER, RON"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 221
        assert isinstance(r['data'][0]["numRatings"], str)
        assert float(r['data'][0]["numRatings"]) == 3.0
        assert isinstance(r['data'][0]["overallRating"], str)
        assert float(r['data'][0]["overallRating"]) == 4.2

    def test_8(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'1',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Professors/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "SHANK, GERALD C"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 1
        assert isinstance(r['data'][0]["numRatings"], str)
        assert float(r['data'][0]["numRatings"]) == 3.0
        assert isinstance(r['data'][0]["overallRating"], str)
        assert float(r['data'][0]["overallRating"]) == 4.7

    def tearDown(self):
        pass

if __name__ == "__main__":
    unittest.main()