import requests
import unittest
import random
import re

#unit tests for the python code using unittest

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        pass
    
    #These are tests that check to see if we are getting the information of the requirements for majors correctly.
    #It tests for requirements for a major that exists in our database. It checks for the name of our requirements,
    #number of hours that each requirement contributes to the major, the type of requirement, and every piece of data 
    #used for major requirements. Checking data from our database in random, no sequential order.
    def test_0(self):
        #checks a specific requirement
        params = {
            'filter_by':'pkey',
            'filter_string':'1',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Requirements/',params=params).json()
        #checks the name of this requirement
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "GLOBAL CULTURES"  
         #a unique number given to each requirement      
        assert isinstance(r['data'][0]["pkey"], int)                           
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 1
        #checks the number of hours needed to fulfill this requirement
        assert isinstance(r['data'][0]["numHours"], int)    
        assert r['data'][0]["numHours"] > 0
        assert r['data'][0]["numHours"] == 3
        #checks the link in where we can get more information about this requirement
        assert isinstance(r['data'][0]["web"], str)                  
        assert r['data'][0]["web"] == "https://ugs.utexas.edu/flags/students/about/global-cultures"
        #checks the type of this requirement
        assert isinstance(r['data'][0]["typeOfRequirement"], str)               
        assert r['data'][0]["typeOfRequirement"] == "flag"
        #checks if this requirement has to be taken in residence or not
        assert isinstance(r['data'][0]["inResidence"], str)               
        assert r['data'][0]["inResidence"] == "yes"
        #checks to see if there are classes that fulfill this requirement
        x = r['data'][0]["exampleClasses"].split(", ")    
        assert len(x) > 0      

    def test_1(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'2',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Requirements/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "ETHICS"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 2
        assert isinstance(r['data'][0]["numHours"], int)    
        assert r['data'][0]["numHours"] > 0
        assert r['data'][0]["numHours"] == 3
        assert isinstance(r['data'][0]["web"], str)                  
        assert r['data'][0]["web"] == "https://ugs.utexas.edu/flags/students/about/ethics"
        assert isinstance(r['data'][0]["typeOfRequirement"], str)               
        assert r['data'][0]["typeOfRequirement"] == "flag"
        assert isinstance(r['data'][0]["inResidence"], str)               
        assert r['data'][0]["inResidence"] == "yes"
        x = r['data'][0]["exampleClasses"].split(", ")                        
        assert len(x) > 0

    def test_2(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'3',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Requirements/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "CULTURAL DIVERSITY"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 3
        assert isinstance(r['data'][0]["numHours"], int)    
        assert r['data'][0]["numHours"] > 0
        assert r['data'][0]["numHours"] == 3
        assert isinstance(r['data'][0]["web"], str)                  
        assert r['data'][0]["web"] == "https://ugs.utexas.edu/flags/students/about/cultural-diversity"
        assert isinstance(r['data'][0]["typeOfRequirement"], str)               
        assert r['data'][0]["typeOfRequirement"] == "flag"
        assert isinstance(r['data'][0]["inResidence"], str)               
        assert r['data'][0]["inResidence"] == "yes"
        x = r['data'][0]["exampleClasses"].split(", ")                        
        assert len(x) > 0

    def test_3(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'4',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Requirements/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "QUANTITATIVE REASONING"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 4
        assert isinstance(r['data'][0]["numHours"], int)    
        assert r['data'][0]["numHours"] > 0
        assert r['data'][0]["numHours"] == 3
        assert isinstance(r['data'][0]["web"], str)                  
        assert r['data'][0]["web"] == "https://ugs.utexas.edu/flags/students/about/quantitative-reasoning"
        assert isinstance(r['data'][0]["typeOfRequirement"], str)               
        assert r['data'][0]["typeOfRequirement"] == "flag"
        assert isinstance(r['data'][0]["inResidence"], str)               
        assert r['data'][0]["inResidence"] == "yes"
        x = r['data'][0]["exampleClasses"].split(", ")                        
        assert len(x) > 0

    def test_4(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'5',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Requirements/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "INDEPENDENT INQUIRY"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 5
        assert isinstance(r['data'][0]["numHours"], int)    
        assert r['data'][0]["numHours"] > 0
        assert r['data'][0]["numHours"] == 3
        assert isinstance(r['data'][0]["web"], str)                  
        assert r['data'][0]["web"] == "https://ugs.utexas.edu/flags/students/about/Independent-inquiry"
        assert isinstance(r['data'][0]["typeOfRequirement"], str)               
        assert r['data'][0]["typeOfRequirement"] == "flag"
        assert isinstance(r['data'][0]["inResidence"], str)               
        assert r['data'][0]["inResidence"] == "yes"
        x = r['data'][0]["exampleClasses"].split(", ")                        
        assert len(x) > 0

    def test_5(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'21',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Requirements/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "SYSTEMS"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 21
        assert isinstance(r['data'][0]["numHours"], int)    
        assert r['data'][0]["numHours"] > 0
        assert r['data'][0]["numHours"] == 8
        assert isinstance(r['data'][0]["web"], str)                  
        assert r['data'][0]["web"] == "https://www.cs.utexas.edu/undergraduate-program/academics/curriculum"
        assert isinstance(r['data'][0]["typeOfRequirement"], str)               
        assert r['data'][0]["typeOfRequirement"] == "Major"
        assert isinstance(r['data'][0]["inResidence"], str)               
        assert r['data'][0]["inResidence"] == "yes"
        x = r['data'][0]["exampleClasses"].split(", ")                        
        assert len(x) > 0

    def test_6(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'16',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Requirements/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "ADDITIONAL SCIENCE"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 16
        assert isinstance(r['data'][0]["numHours"], int)    
        assert r['data'][0]["numHours"] > 0
        assert r['data'][0]["numHours"] == 3
        assert isinstance(r['data'][0]["web"], str)                  
        assert r['data'][0]["web"] == "https://cns.utexas.edu/images/CNS/Deans_Office/Student_Records/Web.DegPlans/2018-20_checklists/BS_CS_OPTION1_1820.pdf"
        assert isinstance(r['data'][0]["typeOfRequirement"], str)               
        assert r['data'][0]["typeOfRequirement"] == "Introductory Science"
        assert isinstance(r['data'][0]["inResidence"], str)               
        assert r['data'][0]["inResidence"] == "yes"
        x = r['data'][0]["exampleClasses"].split(", ")                        
        assert len(x) > 0

    def test_7(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'11',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Requirements/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "AMERICAN HISTORY"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 11
        assert isinstance(r['data'][0]["numHours"], int)    
        assert r['data'][0]["numHours"] > 0
        assert r['data'][0]["numHours"] == 6
        assert isinstance(r['data'][0]["web"], str)                  
        assert r['data'][0]["web"] == "https://catalog.utexas.edu/general-information/academic-policies-and-procedures/core-curriculum/"
        assert isinstance(r['data'][0]["typeOfRequirement"], str)               
        assert r['data'][0]["typeOfRequirement"] == "core"
        assert isinstance(r['data'][0]["inResidence"], str)               
        assert r['data'][0]["inResidence"] == "yes"
        x = r['data'][0]["exampleClasses"].split(", ")                        
        assert len(x) > 0

    def test_8(self):
        params = {
            'filter_by':'pkey',
            'filter_string':'19',
            'sort_by':''
        }
        r = requests.get('http://0.0.0.0:80/api/Requirements/',params=params).json()
        assert isinstance(r['data'][0]["name"], str)
        assert r['data'][0]["name"] == "PROGRAMMING"
        assert isinstance(r['data'][0]["pkey"], int)
        assert r['data'][0]["pkey"] > 0
        assert r['data'][0]["pkey"] == 19
        assert isinstance(r['data'][0]["numHours"], int)    
        assert r['data'][0]["numHours"] > 0
        assert r['data'][0]["numHours"] == 6
        assert isinstance(r['data'][0]["web"], str)                  
        assert r['data'][0]["web"] == "https://www.cs.utexas.edu/undergraduate-program/academics/curriculum"
        assert isinstance(r['data'][0]["typeOfRequirement"], str)               
        assert r['data'][0]["typeOfRequirement"] == "Major"
        assert isinstance(r['data'][0]["inResidence"], str)               
        assert r['data'][0]["inResidence"] == "yes"
        x = r['data'][0]["exampleClasses"].split(", ")                        
        assert len(x) > 0

    def tearDown(self):
        pass

if __name__ == "__main__":
    unittest.main()
