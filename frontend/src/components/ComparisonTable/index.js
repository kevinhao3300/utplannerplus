import React, { useEffect } from "react";
import { lighten, makeStyles } from "@material-ui/core/styles";

/*  Router  */
import { BrowserRouter as Link, useHistory } from "react-router-dom";

/*  Depth Component  */
import Paper from "@material-ui/core/Paper";

/*  Table Components  */
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import TablePagination from "@material-ui/core/TablePagination";

const ComparisonTable = (props) => {
  const { rows, data } = props;
  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              {data.map((datum) => (
                <TableCell key={datum.pkey}>{datum.name}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => {
              return (
                <TableRow hover tabIndex={-1} key={row}>
                  {data.map((datum) => {
                    const value = datum[row];
                    return (
                      <TableCell key={datum.pkey} style={{ cursor: "pointer" }}>
                        <span>{value}</span>
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};
