import React from "react";

const KPICard = (props) => {
  const { title, value, width } = props;
  const style = { width: `${width}%` };
  //
  return (
    <div className="card">
      <div className="card-body text-center">
        <h4>{title}</h4>
        <div className="display-4 font-weight-bold mb-4">{value}</div>
        <div className="progress progress-sm">
          <div
            className="progress-bar bg-orange"
            role="progressbar"
            style={style}
            aria-valuenow={width}
            aria-valuemin="0"
            aria-valuemax="100"
          ></div>
        </div>
      </div>
    </div>
  );
};

export default KPICard;
