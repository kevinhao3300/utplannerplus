import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));
const SearchBar = (props) => {
  const { input, placeholder, onChange, onSubmit } = props;
  const classes = useStyles();

  // renders search bar
  return (
    <Paper className={classes.root}>
      <InputBase
        className={classes.input}
        value={input}
        placeholder={placeholder}
        onChange={onChange}
        onKeyPress={onSubmit}
      />
      <Divider className={classes.divider} orientation="vertical" />
      <IconButton
        type="submit"
        className={classes.iconButton}
        aria-label="search"
        onClick={() => onSubmit({ key: "Enter" })}
      >
        <SearchIcon />
      </IconButton>
    </Paper>
  );
};

export default SearchBar;
