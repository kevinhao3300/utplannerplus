import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";

const useStyles = makeStyles((theme) => ({
  root: {
    marginLeft: "auto",
    marginRight: "auto",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

const LoadingBar = () => {
  const classes = useStyles();
  // renders loading bar
  return (
    <div className={classes.root}>
      <h4>Loading...</h4>
      <LinearProgress color="secondary" />
    </div>
  );
};

export default LoadingBar;
