import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

/*  Router  */
import { useHistory } from "react-router-dom";

/*  Depth Component  */
import Paper from "@material-ui/core/Paper";

/*  Table Components  */
import T from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import TablePagination from "@material-ui/core/TablePagination";

/*  Search Highlighter  */
import Highlighter from "react-highlight-words";

import LoadingBar from "../LoadingBar";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  container: { maxHeight: 2000 },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
});

export const Table = (props) => {
  const { columns, query, route, filter = {} } = props;
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [total, setTotal] = useState(0);
  const [pageData, setPageData] = useState([]);
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("");
  const [pageNumber, setPageNumber] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  useEffect(() => {
    const getPage = async () => {
      setLoading(true);
      // API call
      const requirements = await query(
        filter.column,
        filter.input[filter.column],
        orderBy,
        order === "asc" ? "False" : "True",
        pageNumber + 1,
        rowsPerPage
      );
      setTotal(requirements.count);
      setPageData(requirements.data);
      setLoading(false);
    };
    getPage();
  }, [
    order,
    orderBy,
    pageNumber,
    rowsPerPage,
    filter.column,
    filter.input,
    query,
  ]);

  const EnhancedTableHead = ({
    classes,
    order,
    orderBy,
    rowCount,
    onRequestSort,
  }) => {
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };

    // renders sortable table
    return (
      <TableHead>
        <TableRow>
          {columns.map((column) => (
            <TableCell
              key={column.id}
              sortDirection={orderBy === column.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === column.id}
                direction={orderBy === column.id ? order : "asc"}
                onClick={createSortHandler(column.id)}
              >
                {column.label}
                {orderBy === column.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === "desc"
                      ? "sorted descending"
                      : "sorted ascending"}
                  </span>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPageNumber(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPageNumber(0);
  };
  const history = useHistory();

  return (
    <Paper className={classes.root}>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={total}
        rowsPerPage={rowsPerPage}
        page={pageNumber}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      <TableContainer className={classes.container}>
        <T stickyHeader aria-label="sticky table">
          <EnhancedTableHead
            classes={classes}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            rowCount={total}
          />

          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={columns.length} align="center">
                  <LoadingBar />
                </TableCell>
              </TableRow>
            ) : (
              pageData.map((row) => {
                return (
                  <TableRow
                    style={{ cursor: "pointer" }}
                    hover
                    tabIndex={-1}
                    key={row.code}
                  >
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell
                          key={column.id}
                          align={column.align}
                          onClick={() => {
                            if (route === "courses") {
                              if (column.id === "req") {
                                if (row.req !== "NONE")
                                  history.push(
                                    `/requirements/name/${
                                      row.req.split(", ")[0]
                                    }`
                                  );
                              } else if (column.id === "instructor")
                                history.push(
                                  `/professors/name/${row.instructor}`
                                );
                              else history.push(`/${route}/pkey/${row.pkey}`);
                            } else history.push(`/${route}/pkey/${row.pkey}`);
                          }}
                        >
                          {(filter.column === column.id &&
                            filter.input[filter.column]) ||
                          filter.column === "all" ? (
                            <Highlighter
                              searchWords={filter.input[filter.column].split(
                                " "
                              )}
                              autoEscape={false}
                              textToHighlight={value.toString()}
                            />
                          ) : (
                            <span>{value}</span>
                          )}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })
            )}
          </TableBody>
        </T>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={total}
        rowsPerPage={rowsPerPage}
        page={pageNumber}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
};

export default Table;
