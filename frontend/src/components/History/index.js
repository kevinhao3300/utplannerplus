/*  History component  */
import { createBrowserHistory } from "history";

/*  export portion that allows history push  */
export default createBrowserHistory();
