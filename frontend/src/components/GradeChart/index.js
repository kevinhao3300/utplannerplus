import React from "react";
import { Card, CardBody, Col, Row } from "reactstrap";
import { Bar } from "react-chartjs-2";

// random values for demo
let rFactor = function () {
  return Math.round(Math.random() * 100);
};

//Bar chart

export const BarChart = () => {
  let barData = {
    labels: ["A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "D", "D-", "F"],
    datasets: [
      {
        label: "Grade Distribution",
        backgroundColor: "#1e88e5",
        borderColor: "#1e88e5",
        data: [
          rFactor(),
          rFactor(),
          rFactor(),
          rFactor(),
          rFactor(),
          rFactor(),
          rFactor(),
          rFactor(),
          rFactor(),
          rFactor(),
          rFactor(),
          rFactor(),
          rFactor(),
        ],
      },
    ],
  };
  return (
    <Card>
      <CardBody>
        <Row>
          <Col lg="12">
            <div className="campaign ct-charts">
              <div
                className="chart-wrapper"
                style={{ width: "100%", margin: "0 auto", height: 250 }}
              >
                <Bar
                  data={barData}
                  options={{
                    maintainAspectRatio: false,
                    legend: {
                      display: true,
                      labels: { fontFamily: "Nunito Sans" },
                    },
                    scales: {
                      yAxes: [
                        {
                          gridLines: { display: false },
                          ticks: { fontFamily: "Nunito Sans" },
                        },
                      ],
                      xAxes: [
                        {
                          gridLines: { display: false },
                          ticks: { fontFamily: "Nunito Sans" },
                          barThickness: 15,
                        },
                      ],
                    },
                  }}
                />
              </div>
            </div>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

export default BarChart;
