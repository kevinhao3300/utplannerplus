import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));
const Search = (props) => {
  const { input, placeholder, onChange } = props;
  const classes = useStyles();

  // renders Search component
  return (
    <Paper className={classes.root}>
      <InputBase
        className={classes.input}
        value={input}
        placeholder={placeholder}
        onChange={onChange}
      />
    </Paper>
  );
};

export default Search;
