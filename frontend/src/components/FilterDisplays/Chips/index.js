import React from "react";
import Chip from "@material-ui/core/Chip";
import "./index.css";

const Chips = (props) => {
  // renders filter chips
  const { chips, activeChips, handleClick } = props;
  return (
    <div className="chip-container">
      <ul>
        {chips.map((chip) => {
          return (
            <li key={chip}>
              <Chip
                variant={activeChips.includes(chip) ? "default" : "outlined"}
                label={chip}
                onClick={() => handleClick(chip)}
              />
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Chips;
