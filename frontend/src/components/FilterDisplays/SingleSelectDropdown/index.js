import React from "react";
import FormControl from "@material-ui/core/FormControl";
import NativeSelect from "@material-ui/core/NativeSelect";
import InputBase from "@material-ui/core/InputBase";

const SingleSelectDropdown = (props) => {
  const { currentOption, options, onChange } = props;
  return (
    // renders drop down menu
    <FormControl>
      <NativeSelect
        value={currentOption}
        onChange={onChange}
        input={<InputBase />}
      >
        {options.map((option) => (
          <option value={option}>{option}</option>
        ))}
      </NativeSelect>
    </FormControl>
  );
};

export default SingleSelectDropdown;
