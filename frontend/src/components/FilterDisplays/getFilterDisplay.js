import React from "react";
export const FILTER_DISPLAYS = {
  CHIP: "chip",
  RADIO: "radio",
  SEARCH: "search",
  SINGLE_SELECT_DROPDOWN: "single select dropdown",
};

/*  column is an object  */
export const getFilterDisplay = (column, filter) => {
  // conditional rendering of filter display based on type
  switch (column.type) {
    // dropdown menu
    case FILTER_DISPLAYS.SINGLE_SELECT_DROPDOWN:
      return (
        <>
          <div>
            <h5 className="card-title">{column.label}</h5>
            <SingleSelectDropdown
              currentOption={filter}
              options={column.options}
              onChange={(event) => handleFilterChange(i, event)}
            />
          </div>
          <br />
        </>
      );
    // radio button
    case FILTER_DISPLAYS.RADIO:
      return (
        <>
          <div>
            <h5 className="card-title">{column.label}</h5>
            <RadioButtons
              currentOption={filter}
              options={column.options}
              onChange={(event) => handleFilterChange(i, event)}
            />
          </div>
          <br />
        </>
      );
    // search
    case FILTER_DISPLAYS.SEARCH:
      return (
        <>
          <div>
            <h5 className="card-title">{column.label}</h5>
            <Search
              input={filter}
              placeholder={column.placeholder}
              onChange={(event) => handleFilterChange(i, event)}
            />
          </div>
          <br />
        </>
      );
    default:
  }
};

export default { getFilterDisplay, FILTER_DISPLAYS };
