import React, { useState, useEffect } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";

/*  Layout components  */
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";

import { makeStyles } from "@material-ui/core/styles";

import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import TablePagination from "@material-ui/core/TablePagination";
import LoadingBar from "../LoadingBar";

const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
}));

export const CardGrid = (props) => {
  // state variables
  const { columns, query, filter = {}, cardComponent } = props;
  const classes = useStyles();
  const [total, setTotal] = useState(0);
  const [pageData, setPageData] = useState([]);
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("");
  const [pageNumber, setPageNumber] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(15);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    // api call
    const getData = async () => {
      setLoading(true);
      const requirements = await query(
        filter.column,
        filter.input[filter.column],
        orderBy,
        order === "asc" ? "False" : "True",
        pageNumber + 1,
        rowsPerPage
      );
      setTotal(requirements.count);
      setPageData(requirements.data);
      setLoading(false);
    };
    getData();
  }, [
    order,
    orderBy,
    pageNumber,
    rowsPerPage,
    filter.column,
    filter.input,
    query,
  ]);

  const EnhancedTableHead = ({ classes, order, orderBy, onRequestSort }) => {
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };

    return (
      // renders cells that can handle sorting
      <TableRow>
        {columns.map((column) => (
          <TableCell
            key={column.id}
            sortDirection={orderBy === column.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === column.id}
              direction={orderBy === column.id ? order : "asc"}
              onClick={createSortHandler(column.id)}
            >
              {column.label}
              {orderBy === column.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    );
  };

  // change handlers
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPageNumber(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPageNumber(0);
  };

  return (
    <>
      {/* render container that handles pagination */}
      <div className="card">
        <EnhancedTableHead
          classes={classes}
          order={order}
          orderBy={orderBy}
          onRequestSort={handleRequestSort}
        />
        <TablePagination
          rowsPerPageOptions={[15, 45, 150]}
          component="div"
          count={total}
          rowsPerPage={rowsPerPage}
          page={pageNumber}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
        <CssBaseline />
        <Container className={classes.cardGrid}>
          {loading ? (
            <LoadingBar />
          ) : (
            <Grid container spacing={4}>
              {pageData.map((data) => cardComponent(data, filter))}
            </Grid>
          )}
        </Container>
        <TablePagination
          rowsPerPageOptions={[15, 45, 150]}
          component="div"
          count={total}
          rowsPerPage={rowsPerPage}
          page={pageNumber}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </div>
    </>
  );
};

export default CardGrid;
