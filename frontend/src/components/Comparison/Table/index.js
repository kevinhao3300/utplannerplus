import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

/*  Depth Component  */
import Paper from "@material-ui/core/Paper";

/*  Table Components  */
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  container: { maxHeight: 2000 },
});

const ComparisonTable = (props) => {
  const classes = useStyles();
  const { title, query, rows, pkeys, hide } = props;
  const [data, setData] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const values = new Array(pkeys.length);
      for (let i = 0; i < values.length; i++) {
        // make api c
        const value = await query("pkey", pkeys[i]);
        values[i] = value.data[0];
      }
      setData(values);
    };
    getData();
  }, [pkeys, query]);
  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell>
                <Button variant="contained" onClick={() => hide(false, pkeys)}>
                  Hide Comparison
                </Button>
              </TableCell>
              {data.map((datum, i) => (
                <TableCell key={i}>
                  {title} {i + 1}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => {
              return (
                <TableRow hover tabIndex={-1} key={row}>
                  <TableCell key={row.id}>{row.label}</TableCell>
                  {data.map((datum) => {
                    const value = datum[row.id];
                    return (
                      <TableCell key={datum.pkey} style={{ cursor: "pointer" }}>
                        <span>{value}</span>
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

export default ComparisonTable;
