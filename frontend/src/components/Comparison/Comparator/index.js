import React, { useState } from "react";
import Slider from "@material-ui/core/Slider";
import Search from "../../FilterDisplays/Search";
import Button from "@material-ui/core/Button";
import ErrorAlertDialog from "../ErrorAlert";

const marks = Array.from(Array(3).keys()).map((i) => {
  return { value: i + 2, label: i + 2 };
});
const Comparator = (props) => {
  const { title, onComparison, count } = props;
  const [error, setError] = useState(false);
  const [size, setSize] = useState(2);
  const [keys, setKeys] = useState(
    Array.from(Array(size).keys()).map((i) => "")
  );
  const placeholder = `Enter ${title} key number...`;

  // change handlers
  const handleSizeChange = (event, newValue) => {
    setSize(newValue);
  };

  const handleComparatorChange = (key, event) => {
    const newKeys = [...keys];
    newKeys[key] = event.target.value;
    setKeys(newKeys);
  };

  const handleCompareButton = () => {
    const checked = keys
      .slice(0, size)
      .filter(
        (key) => key > 0 && key <= count && Number.isInteger(Number(key))
      );
    console.log(checked.length, count);
    if (checked.length === size) onComparison(true, keys.slice(0, size));
    else setError(true);
  };

  return (
    <>
      {/* Handles the selection of the objects to compare */}
      <div className="card">
        <h4 className="card-header text-left font-weight-bold">
          Compare {title}s
        </h4>
        <div className="card-body">
          <h5 className="card-title">Number of Comparisons</h5>
          <Slider
            defaultValue={2}
            aria-labelledby="discrete-slider-always"
            marks={marks}
            onChange={handleSizeChange}
            min={2}
            max={4}
          />
        </div>
        <div className="card-body">
          {Array.from(Array(size).keys()).map((i) => {
            return (
              <>
                <div>
                  <h5 className="card-title">
                    {title} {i + 1}
                  </h5>
                  <Search
                    input={keys[i]}
                    placeholder={placeholder}
                    onChange={(event) => handleComparatorChange(i, event)}
                  />
                </div>
                <br />
              </>
            );
          })}
        </div>
        {/* compares the selected instances */}
        <Button variant="contained" onClick={handleCompareButton}>
          Compare values
        </Button>
      </div>
      <ErrorAlertDialog open={error} onClose={setError} />
    </>
  );
};

export default Comparator;
