import React from "react";

/* Router Components */
import { Link } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";

/* App Bar */
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import STYLES from "../../constants/Fonts";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    "&:visited": {
      color: "white",
      textDecoration: "none",
    },
    "&:link": {
      textDecoration: "none",
    },
    "&:active": {
      textDecoration: "none",
    },
    "&:hover": {
      textDecoration: "none",
    },
  },
  options: {
    fontFamily: STYLES.NAVIGATION.FONT,
    fontSize: STYLES.NAVIGATION.SIZE,
    color: "white",
    textDecoration: "none",
  },
  offset: theme.mixins.toolbar,
}));

/**
 *
 * @param {Array} links takes in an array of objects
 *                      format is
 *                      {
 *                        title:
 *                        route:
 *                      }
 */

const NavigationBar = ({ links }) => {
  const classes = useStyles();
  // render navigation bar
  return (
    <AppBar position="static">
      <Toolbar>
        <Link to="/" className={classes.title}>
          <Typography className={classes.options}>UT Planner Plus</Typography>
        </Link>

        <Tabs>
          {links
            .filter((link) => link.title !== "")
            .map((link) => {
              return (
                <Link to={`/${link.title}`}>
                  <Tab className={classes.options} label={link.title} />
                </Link>
              );
            })}
        </Tabs>
      </Toolbar>
    </AppBar>
  );
};

export default NavigationBar;
