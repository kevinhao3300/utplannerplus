import React from "react";
import { Route, Switch } from "react-router-dom";
import { About } from "./pages/About";
import { Home } from "./pages/Home";
import { Courses } from "./pages/Courses";
import Course from "./pages/Courses/_components/Course";
import { Professors } from "./pages/Professors";
import Professor from "./pages/Professors/_components/Professor";
import { Requirements } from "./pages/Requirements";
import { Requirement } from "./pages/Requirements/_components/Requirement";
import { Search } from "./pages/Search";
import { Visualizations } from "./pages/Visualizations";
// import { ProviderVisualizations } from "./pages/ProviderVisualizations";

// This Route will wrap around our entire app
function Routes() {
  return (
    <Switch>
      {/* Home page route */}
      <Route exact path="/">
        <Home />
      </Route>
      {/* About page route */}
      <Route exact path="/about">
        <About />
      </Route>
      {/* Classes page route */}
      <Route exact path="/courses">
        <Courses />
      </Route>
      <Route
        exact
        path="/courses/:by/:string"
        render={(props) => {
          return (
            <Course
              filters={{
                by: props.match.params.by,
                string: props.match.params.string,
              }}
            />
          );
        }}
      ></Route>
      {/* Professors page route */}
      <Route exact path="/professors">
        <Professors />
      </Route>
      <Route
        exact
        path="/professors/:by/:string"
        render={(props) => {
          return (
            <Professor
              filters={{
                by: props.match.params.by,
                string: props.match.params.string,
              }}
            />
          );
        }}
      ></Route>
      {/* REquirements page route */}
      <Route exact path="/requirements">
        <Requirements />
      </Route>
      <Route
        exact
        path="/requirements/:by/:string"
        render={(props) => {
          return (
            <Requirement
              filters={{
                by: props.match.params.by,
                string: props.match.params.string,
              }}
            />
          );
        }}
      ></Route>
      {/* Search page route */}
      <Route
        exact
        path="/search/:searchText"
        render={(props) => {
          return <Search searchText={props.match.params.searchText} />;
        }}
      ></Route>
      {/* Visualization page route */}
      <Route exact path="/visualizations">
        <Visualizations />
      </Route>
    </Switch>
  );
}

export default Routes;
