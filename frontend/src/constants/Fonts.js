export const STYLE = {
  TITLE: {
    FONT: "Playfair Display",
    SIZE: 100, 
  },
  HEADING: {
    FONT: "Playfair Display",
    SIZE: 36
  },
  SUBHEADING: {
    FONT: "Playfair Display",
    SIZE: 24
  },
  NAVIGATION: {
    FONT: "Playfair Display",
    SIZE: 16
  },
  CAPTION: {
    FONT: "Montserrate",
    SIZE: 20
  },
  BODY: {
    FONT: "Montserrat",
    SIZE: 12
  },
};

export default STYLE;
