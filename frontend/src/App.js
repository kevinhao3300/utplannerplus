import React from "react";
import "./App.css";

/* Components */
import NavigationBar from "./components/NavigationBar";

/* Pages imports */
import { Home } from "./pages/Home";
import { About } from "./pages/About";
import { Professors } from "./pages/Professors";
import Courses from "./pages/Courses";
import { Requirements } from "./pages/Requirements";
import { Visualizations } from "./pages/Visualizations";
import Routes from "./routes";

// pages routable from the nav bar
const PAGES = [
  {
    title: "about",
    route: <About />,
  },
  {
    title: "visualizations",
    route: <Visualizations />,
  },
  {
    title: "professors",
    route: <Professors />,
  },
  {
    title: "courses",
    route: <Courses />,
  },
  {
    title: "requirements",
    route: <Requirements />,
  },
  {
    title: "",
    route: <Home />,
  },
];

function App() {
  return (
    <>
      <NavigationBar links={PAGES} />
      <Routes />
    </>
  );
}

export default App;
