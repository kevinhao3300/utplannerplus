import axios from "axios";
import wrapper from "axios-cache-plugin";

// wrap axios so we can cache API requests
let http = wrapper(axios, {
  maxCacheSize: 30,
});

http.__addFilter(/api/);

export default http;
