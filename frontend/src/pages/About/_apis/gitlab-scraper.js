import http from "../../../API/HTTP";

const COMMIT_URL =
  "https://gitlab.com/api/v4/projects/21436298/repository/commits";
const ISSUE_URL = "https://gitlab.com/api/v4/projects/21436298/issues";

export const fetchGitLabCommits = async () => {
  let response;
  try {
    response = await http.get(COMMIT_URL, {
      params: {
        all: true,
        per_page: 100,
      },
    });
  } catch (err) {
    return [];
  }
  const data = response.data;
  return data;
};

export const fetchGitLabIssues = async () => {
  let response;
  try {
    response = await http.get(ISSUE_URL);
  } catch (err) {
    return [];
  }
  const data = response.data;
  return data;
};

export default {
  fetchGitLabCommits,
  fetchGitLabIssues,
};
