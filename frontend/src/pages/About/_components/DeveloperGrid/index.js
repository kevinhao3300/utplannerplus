import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
// Gitlab scraper
import {
  fetchGitLabCommits,
  fetchGitLabIssues,
} from "../../_apis/gitlab-scraper";
import DeveloperCard from "../DeveloperCard";

class DeveloperGrid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      developers: props.info,
      totalCommits: 0,
      totalIssues: 0,
      totalTests: 0,
    };
  }

  componentDidMount() {
    const developers = this.state.developers;
    const fetchData = async () => {
      let commitData;
      let issueData;
      let numCommits = 0;
      let numIssues = 0;
      try {
        // get gitlab information from API
        commitData = await fetchGitLabCommits();
        issueData = await fetchGitLabIssues();
        let developersCommitCount = {};
        let developersIssueCount = {};
        commitData.forEach((commit) => {
          numCommits++;
          const count = developersCommitCount[commit.author_name];
          developersCommitCount[commit.author_name] = count ? count + 1 : 1;
        });
        issueData.forEach((issue) => {
          const count = developersIssueCount[issue.assignee];
          if (issue.closed_at) numIssues++;
          if (issue.assignee && issue.assignee.name) {
            developersIssueCount[issue.assignee.name] = count ? count + 1 : 1;
          }
        });
        developers.forEach((developer) => {
          if (developersCommitCount[developer.name])
            developer.commits = developersCommitCount[developer.name];
          if (developersIssueCount[developer.name])
            developer.issues = developersIssueCount[developer.name];
        });
        this.setState({
          developers: developers,
          totalCommits: numCommits,
          totalIssues: numIssues,
        });
      } catch (err) {
        throw err;
      }
    };
    fetchData();
  }

  render() {
    const { developers, totalCommits, totalIssues, totalTests } = this.state;
    return (
      <>
        <div
          style={{
            display: "flex",
            flex: 1,
            flexDirection: "row",
            justifyContent: "space-around",
            marginTop: "18px",
          }}
        >
          {/* render a developer card for each developer */}
          {developers.map((developer) => (
            <DeveloperCard
              name={developer.name}
              photo={developer.photo}
              bio={developer.bio}
              responsibilities={developer.responsibilities}
              commits={developer.commits}
              issues={developer.issues}
              tests={developer.tests}
            />
          ))}
        </div>
        <div
          style={{
            paddingLeft: "33%",
            marginTop: "18px",
            paddingBottom: "5px",
            textAlign: "center",
          }}
        >
          <Card variant="outlined" style={{ height: 235, width: 600 }}>
            <h4 style={{ fontSize: "28px", margin: "3%" }}>GitLab Data</h4>
            <CardActions style={{ flex: 1, paddingLeft: "22.5%" }}>
              <Button variant="contained" size="small" color="primary">
                Commits: {totalCommits}
              </Button>
              <Button variant="contained" size="small" color="primary">
                Issues: {totalIssues}
              </Button>
              <Button variant="contained" size="small" color="primary">
                tests: {totalTests}
              </Button>
            </CardActions>
            <div style={{ textAlign: "center" }}>
              <a href="https://gitlab.com/kevinhao3300/utplannerplus">
                <img
                  src={require("../../_images/icons/gitlab.png")}
                  width="100rem"
                  height="100rem"
                  alt={"Gitlab Icon"}
                ></img>
              </a>
            </div>
          </Card>
        </div>
      </>
    );
  }
}

export default DeveloperGrid;
