import React from "react";

/* Card Components */
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";

//used to change the color of Button
const theme = createMuiTheme({
  overrides: {
    // Style sheet name ⚛️
    MuiButton: {
      // Name of the rule
      contained: {
        // Some CSS
        background: "linear-gradient(45deg, #fc9403 30%, #fc8803 60%)",
        borderRadius: 3,
        border: 0,
        color: "white",
        height: 48,
        padding: "0 30px",
        boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
      },
    },
  },
});

// returns a Developer Card
export const DeveloperCard = ({
  name,
  photo,
  bio,
  responsibilities,
  commits,
  issues,
  tests,
}) => {
  return (
    <Card style={{ height: 540, width: 300 }}>
      <CardActionArea>
        <CardMedia
          style={{ height: 300, width: 300 }}
          className={""}
          image={photo}
        />
        <CardContent style={{ height: 163, width: 300 }}>
          <Typography gutterBottom variant="h5" component="h2">
            {name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="h3">
            {responsibilities}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {bio}
          </Typography>
          <div style={{ display: "flex", flex: 1, flexDirection: "row" }}></div>
        </CardContent>
      </CardActionArea>
      <CardActions style={{ flex: 1 }}>
        <ThemeProvider theme={theme}>
          <Button variant="contained" size="small" color="primary">
            Commits: {commits}
          </Button>
          <Button variant="contained" size="small" color="primary">
            Issues: {issues}
          </Button>
          <Button variant="contained" size="small" color="primary">
            tests: {tests}
          </Button>
        </ThemeProvider>
      </CardActions>
    </Card>
  );
};

export default DeveloperCard;
