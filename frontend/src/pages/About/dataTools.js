import React from "react";
import { Tab, Col, ListGroup } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { SOURCES } from "./constants";
import { makeStyles } from "@material-ui/core/styles";

// styles to fix the content of data, tools, and other
const useStyles = makeStyles((theme) => ({
  tabContent: {
    paddingLeft: "100px",
    marginTop: "30px",
    paddingBottom: "5px",
    textAlign: "left",
  },
}));

//used to display our data, tools, and other
export default function MoreInfo() {
  const classes = useStyles();
  return (
    <div style={{ margin: "18px", textAlign: "center", paddingBottom: "50px" }}>
      <br />
      <Tab.Container defaultActiveKey="#data">
        <Col sm={20}>
          <ListGroup horizontal>
            <ListGroup.Item action href="#data">
              Data
            </ListGroup.Item>
            <ListGroup.Item action href="#tools">
              Tools
            </ListGroup.Item>
            <ListGroup.Item action href="#other">
              Other
            </ListGroup.Item>
          </ListGroup>
        </Col>
        <Col sm={20}>
          <Tab.Content>
            <Tab.Pane eventKey="#data" className={classes.tabContent}>
              {/* add data portion */}
              <ul className="list-unstyled">
                <li>
                  <a href="https://registrar.utexas.edu/schedules/archive">
                    <img
                      src={require("./_images/icons/ut.png")}
                      width="100rem"
                      height="100rem"
                      alt={"UT Background"}
                    ></img>
                    UT Registrar
                  </a>
                  : We are going to scrape this link in order to get the classes
                  that were offered in each semester. It also contains data
                  about flags, class times, professors, etc.
                </li>
                <li>
                  <a href="https://github.com/Rodantny/Rate-My-Professor-Scraper-and-Search">
                    <img
                      src={require("./_images/icons/github.png")}
                      width="100rem"
                      height="100rem"
                      alt={"RMP API"}
                    ></img>
                    Rate My Professor API
                  </a>
                  : This API will give us information on the student feedback
                  for a given professor. This will contain data such as
                  difficulty of the class and overall rating of the professor.
                </li>
                <li>
                  <a href="https://github.com/KiboNaku/utreview-backend/tree/master/utreview/routes">
                    <img
                      src={require("./_images/icons/github.png")}
                      width="100rem"
                      height="100rem"
                      alt={"UTReview Github"}
                    ></img>
                    UT Catalyst API
                  </a>
                  : This API will allow us to access grade distributions for
                  previously offered courses at UT Austin.
                </li>
              </ul>
            </Tab.Pane>
            <Tab.Pane eventKey="#tools" className={classes.tabContent}>
              {/* add tools portion */}
              <ul className="list-unstyled">
                <li>
                  <a href="https://www.gitlab.com">
                    <img
                      src={require("./_images/icons/gitlab.png")}
                      width="100rem"
                      height="100rem"
                      alt={"GitLab Icon"}
                    ></img>
                    GitLab
                  </a>
                  : Allows us to use version control in order to add new
                  features and fix bugs.
                </li>
                <li>
                  <a href="https://reactjs.org">
                    <img
                      src={require("./_images/icons/react.png")}
                      width="100rem"
                      height="100rem"
                      alt={"React Icon"}
                    ></img>
                    ReactJS
                  </a>
                  : Frontend framework that allows for efficient rendering of
                  dynamic UI elements.
                </li>
                <li>
                  <a href="https://aws.amazon.com/ec2/">
                    <img
                      src={require("./_images/icons/aws-ec2-logo.png")}
                      width="100rem"
                      height="100rem"
                      alt={"EC2 Icon"}
                    ></img>
                    AWS Ec2
                  </a>
                  : Hosts our website, deploys both the frontend and the backend
                </li>
                <li>
                  <a href="https://www.postman.com">
                    <img
                      src={require("./_images/icons/postman.png")}
                      width="100rem"
                      height="100rem"
                      alt={"Postman Icon"}
                    ></img>
                    Postman
                  </a>
                  : Defines and tests our API and how we are going to be using
                  public RESTFUL APIs.
                </li>
              </ul>
            </Tab.Pane>
            <Tab.Pane eventKey="#other" className={classes.tabContent}>
              {/* add other portion */}
              <ul className="list-unstyled">
                <li>
                  <a href="https://documenter.getpostman.com/view/12917725/TVmFifJz">
                    <img
                      src={require("./_images/icons/postman.png")}
                      width="100rem"
                      height="100rem"
                      alt={"Postman Link Icon"}
                    ></img>
                    Postman URL
                  </a>
                  : Our Postman URL
                </li>
                {SOURCES.map((source) => (
                  <li>
                    <a href={source.link} key={source.link}>
                      {source.icon}
                      {source.text}
                    </a>
                    : Data source used to {source.description}
                  </li>
                ))}
              </ul>
            </Tab.Pane>
          </Tab.Content>
        </Col>
      </Tab.Container>
    </div>
  );
}
