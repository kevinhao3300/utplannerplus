import React from "react";
import DeveloperGrid from "./_components/DeveloperGrid";
import { DEVELOPERS_INFO } from "./constants";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import STYLES from "../../constants/Fonts";
import MoreInfo from "./dataTools";

// styles
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    fontFamily: STYLES.TITLE.FONT,
    fontSize: STYLES.TITLE.SIZE,
    color: "white",
    textDecoration: "underline",
    alignSelf: "center",
  },
  subtext: {
    fontFamily: STYLES.CAPTION.FONT,
    fontSize: STYLES.CAPTION.SIZE,
    color: "white",
    textDecoration: "italic",
  },
  container: {
    width: "100%",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    opacity: "90%",
  },
}));

// About Page
export function About() {
  const classes = useStyles();
  return (
    <>
      <div className="container" style={{ height: "500", width: "500" }}>
        {/* Background image */}
        <img
          className="background-img"
          src={require("../../images/ut/ut_students.jpg")}
          alt="https://media.kvue.com/assets/KVUE/images/c0c94461-137b-47fc-b6c0-32d62fcec7c7/c0c94461-137b-47fc-b6c0-32d62fcec7c7_1920x1080.jpg"
        />
        <div
          className="absolute-centered-box"
          style={{
            backgroundColor: "rgba(191, 87, 0, 0.5)",
            height: "61%",
            width: "127%",
          }}
        >
          {/* Purpose of website blurb */}
          <Typography className={classes.title} variant="h2">
            About
          </Typography>
          <Typography className={classes.subtext} variant="p">
            This website is to allow students to plan their 4 year plan at UT
            Austin. It includes features like class planning <br />
            and integrated information about degree plans. Instead of using many
            different platforms to collect information about <br />
            courses/professors, we consolidate all of this information for your
            convenience.
          </Typography>
        </div>
      </div>
      {/* Display our information */}
      <div className="body">
        <h1 style={{ textAlign: "center", marginTop: "20px" }}>
          Meet the Team
        </h1>
        <DeveloperGrid info={DEVELOPERS_INFO} />
      </div>
      {/* Display our data and tools */}
      <MoreInfo />
    </>
  );
}

export default {
  About,
};
