import React from "react";

/*  update path if developers image path changes */
const DEV_IMAGE_PATH = "./_images/developers/";

/*  responsibilties of each developer  */
export const RESPONSIBILITIES = {
  FULLSTACK: "Fullstack Developer",
  FRONTEND: "Frontend Developer",
  BACKEND: "Backend Developer",
};

/*  url sources for about page  */
export const SOURCES = [
  {
    text: "UT Catalyst Grade Distributions",
    link: "http://utcatalyst.org/grade-distributions",
    description:
      "gather infomation about the grade distibutions of professors and classes",
    icon: (
      <img
        src={require("./_images/icons/catalyst.png")}
        width="100rem"
        height="100rem"
        alt={"Catalyst"}
      />
    ),
  },
  {
    text: "UT Reviews",
    link: "https://www.utexasreview.com/",
    description: "gather reviews about classes and professors",
    icon: (
      <img
        src={require("./_images/icons/review.png")}
        width="100rem"
        height="100rem"
        alt={"Utexas Review"}
      />
    ),
  },
  {
    text: "Rate My Professor",
    link: "https://www.ratemyprofessors.com/",
    description: "gather ratings and reviews about professors",
    icon: (
      <img
        src={require("./_images/icons/rpm.jpg")}
        width="100rem"
        height="100rem"
        alt={"RMP"}
      />
    ),
  },
  {
    text: "UT Schedule Archives",
    link: "https://registrar.utexas.edu/schedules/archive",
    description:
      "gather information courses of semesters of different current and previous years",
    icon: (
      <img
        src={require("./_images/icons/ut.png")}
        width="100rem"
        height="100rem"
        alt={"UT Icon"}
      />
    ),
  },
];

/*  information for each developer  */
export const DEVELOPERS_INFO = [
  {
    name: "Alexy Correa",
    photo: require(`${DEV_IMAGE_PATH}correa_alexy.jpg`),
    bio:
      "Hello, my name is Alexy, and I am a junior from McAllen, TX. I like to watch youtube and exercise.",
    responsibilities: RESPONSIBILITIES.BACKEND,
    commits: 0,
    issues: 0,
    tests: 0,
  },
  {
    name: "Kevin Hao",
    photo: require(`${DEV_IMAGE_PATH}hao_kevin.jpg`),
    bio:
      "Hey guys, my name is Kevin and I am a junior computer science student from UT Austin. I like to lift weights and play games with my friends.",
    responsibilities: RESPONSIBILITIES.FULLSTACK,
    commits: 0,
    issues: 0,
    tests: 0,
  },
  {
    name: "Jackson McClurg",
    photo: require(`${DEV_IMAGE_PATH}mcclurg_jackson.jpg`),
    bio:
      "Hi, my name is Jackson and I’m a junior from Katy, TX. In my free time I like to play basketball and go rock climbing.",
    responsibilities: RESPONSIBILITIES.BACKEND,
    commits: 0,
    issues: 0,
    tests: 0,
  },
  {
    name: "Jordan Wang",
    photo: require(`${DEV_IMAGE_PATH}wang_jordan.jpg`),
    bio:
      "Hi my name is Jordan. I’m a junior from Houston, TX. I like to play basketball.",
    responsibilities: RESPONSIBILITIES.BACKEND,
    commits: 0,
    issues: 0,
    tests: 0,
  },
  {
    name: "Jesse Zou",
    photo: require(`${DEV_IMAGE_PATH}zou_jesse.jpg`),
    bio:
      "I'm a Computer Science major under the Integrated Masters Program. I'm 20 years old, and I love League of Legends.",
    responsibilities: RESPONSIBILITIES.FULLSTACK,
    commits: 0,
    issues: 0,
    tests: 0,
  },
];

export default {
  RESPONSIBILITIES,
  SOURCES,
  DEVELOPERS_INFO,
};
