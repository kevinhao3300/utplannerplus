import http from "../../../API/HTTP";

const REQUIREMENTS_URL = "/api/Requirements/";

// handles all API requests dealing with requirements
export const fetchRequirements = async (
  filterBy = "",
  filterString = "",
  sortBy = "",
  reverse = "False",
  pageNumber = 1,
  pageSize = 1
) => {
  let response;
  try {
    // API call
    if (filterString === "All") {
      filterBy = "";
      filterString = "";
    }
    response = await http.get(REQUIREMENTS_URL, {
      params: {
        page_number: pageNumber,
        page_size: pageSize,
        filter_by: filterBy,
        filter_string: decodeURIComponent(filterString),
        sort_by: sortBy,
        reverse,
      },
    });
  } catch (err) {
    return {
      count: 0,
      data: [],
    };
  }
  const data = response.data;
  return data;
};

export default fetchRequirements;
