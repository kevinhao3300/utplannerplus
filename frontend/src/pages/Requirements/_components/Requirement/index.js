import React, { useState, useEffect } from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import Grid from "@material-ui/core/Grid";
import StarIcon from "@material-ui/icons/StarBorder";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import CircularProgress from "@material-ui/core/CircularProgress";
import CardMedia from "@material-ui/core/CardMedia";
import { useHistory } from "react-router-dom";
import fetchRequirements from "../../_apis/RequirementsApi";

import fetchCourses from "../../../Courses/_apis/CoursesApi";
import Table from "../../../../components/Table";
import { columns as col } from "../../../Courses";
const columns = [...col];
columns.push({ id: "instructor", label: "Professors" });
// styles
const useStyles = makeStyles((theme) => ({
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none",
    },
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbar: {
    flexWrap: "wrap",
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[200]
        : theme.palette.grey[700],
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
  },
  center: {
    position: "relative",
    display: "inline-block",
    marginLeft: "46%",
    left: 20,
    marginTop: "20%",
    top: 20,
  },
}));

// requirements information
const information = [
  { id: "pkey", label: "Key #" },
  {
    id: "inResidence",
    label: "in Residence",
  },
  {
    id: "exampleClasses",
    label: "Classes fulfilling Requirement",
  },
  {
    id: "numHours",
    label: "Hours Needed",
  },
  { id: "typeOfRequirement", label: "Type" },
  {
    id: "fulfilledByMajor",
    label: "Fulfilled by Major",
  },
];

export function Requirement({ filters: { by, string } }) {
  const classes = useStyles();
  const [data, setData] = useState();
  useEffect(() => {
    const getData = async () => {
      // API call
      const requirement = await fetchRequirements(by, string);
      setData(requirement.data[0]);
    };
    getData();
  });

  const history = useHistory();

  // handle changes
  const handleClassClick = (classInstance) => {
    history.push(
      `/courses/name/${encodeURIComponent(
        classInstance[9] !== " "
          ? classInstance.substring(9)
          : classInstance.substring(10)
      )}`
    );
  };

  const handleProfClick = (profInstance) => {
    history.push(`/professors/name/${encodeURIComponent(profInstance)}`);
  };

  return (
    <>
      {!data ? (
        <CircularProgress className={classes.center} />
      ) : (
        <>
          <Container
            maxWidth="sm"
            component="main"
            className={classes.heroContent}
          >
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="textPrimary"
              gutterBottom
            >
              {data.name}
            </Typography>
            {/* render image */}
            <CardMedia
              style={{
                height: 100,
                width: 100,
                marginLeft: "auto",
                marginRight: "auto",
              }}
              className={classes.cardMedia}
              image={data.image}
            />
            <Typography
              variant="h5"
              align="center"
              color="textSecondary"
              component="p"
            >
              <a href={data.web}>More information</a>
            </Typography>
          </Container>
          <Container maxWidth="md" component="main">
            <Grid container spacing={5} alignItems="center">
              {information.map((info) => (
                <Grid
                  item
                  key={info.label}
                  md={
                    info.label === "Classes fulfilling Requirement"
                      ? 6
                      : info.label === "Professors"
                      ? 12
                      : 3
                  }
                >
                  {/* render card */}
                  <Card>
                    <CardHeader
                      title={info.label}
                      subheader={info.subheader}
                      titleTypographyProps={{ align: "center" }}
                      subheaderTypographyProps={{ align: "center" }}
                      action={info.label === "Pro" ? <StarIcon /> : null}
                      className={classes.cardHeader}
                    />
                    <CardContent>
                      <div className={classes.cardPricing}>
                        <Typography
                          component="h2"
                          variant="h3"
                          color="textPrimary"
                        >
                          {info.label !== "Classes fulfilling Requirement" &&
                            info.label !== "Professors" &&
                            data[info.id]}
                        </Typography>
                      </div>
                      <ul>
                        {/* handle list formatted data */}
                        {(data &&
                          info.label === "Classes fulfilling Requirement" &&
                          data[info.id].split(", ").map((line) => (
                            <Typography
                              component="li"
                              variant="subtitle1"
                              align="center"
                              key={line}
                              onClick={() => handleClassClick(line)}
                            >
                              {line}
                            </Typography>
                          ))) ||
                          // create clickable professor names
                          (info.label === "Professors" &&
                            data[info.id].map((line) => (
                              <Typography
                                component="li"
                                variant="subtitle1"
                                align="center"
                                key={line}
                                onClick={() => handleProfClick(line)}
                              >
                                {line}
                              </Typography>
                            )))}
                      </ul>
                    </CardContent>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </Container>
          {data.exampleClasses.split(", ").map((className) => (
            <Table
              columns={columns}
              query={fetchCourses}
              route={"courses"}
              filter={{
                column: "name",
                input: {
                  name:
                    className[9] !== " "
                      ? className.substring(9)
                      : className.substring(10),
                },
              }}
            />
          ))}
        </>
      )}
    </>
  );
}

export default Requirement;
