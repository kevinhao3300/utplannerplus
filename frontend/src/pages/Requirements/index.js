import React, { Component } from "react";
import Table from "../../components/Table";
import fetchRequirements from "./_apis/RequirementsApi";
import Filters from "./_components/Filters";
import Comparator from "../../components/Comparison/Comparator";
import ComparisonTable from "../../components/Comparison/Table";

// formatting for requirements table
export const columns = [
  { id: "pkey", label: "Key #", minWidth: 170 },
  { id: "name", label: "Name", minWidth: 170 },
  { id: "inResidence", label: "In Residence", minWidth: 170 },
  { id: "numHours", label: "Hours Needed", minWidth: 170 },
  { id: "typeOfRequirement", label: "Type", minWidth: 170 },
  {
    id: "fulfilledByMajor",
    label: "Fulfilled by Major",
    minWidth: 170,
  },
];

export class Requirements extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: { column: "", input: {} },
      comparison: {
        visible: false,
        pkeys: [],
      },
    };
  }
  handleFilterChange = (column, input) => {
    this.setState({ filter: { column, input } });
  };

  handleComparatorChange = (visible, pkeys) => {
    this.setState({ comparison: { visible, pkeys } });
  };

  render() {
    const {
      filter,
      comparison: { visible, pkeys },
    } = this.state;

    return (
      <>
        <h2>Requirements</h2>
        <h3>
          Click on the requirements below to get more information about them.
        </h3>
        {/* Render requirements table */}
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-3">
              <Filters onFilterChange={this.handleFilterChange} />
              <br />
              <Comparator
                title={"Requirement"}
                onComparison={this.handleComparatorChange}
                count={23}
              />
            </div>
            <div className="col-md-9">
              {!visible ? (
                <Table
                  columns={columns}
                  query={fetchRequirements}
                  route={"requirements"}
                  filter={filter}
                />
              ) : (
                <ComparisonTable
                  query={fetchRequirements}
                  title={"Requirement"}
                  route={"requirements"}
                  rows={columns}
                  pkeys={pkeys}
                  hide={this.handleComparatorChange}
                />
              )}
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Requirements;
