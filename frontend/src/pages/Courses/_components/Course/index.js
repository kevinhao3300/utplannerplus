import React, { useState, useEffect } from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import StarIcon from "@material-ui/icons/StarBorder";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import CircularProgress from "@material-ui/core/CircularProgress";
import GradeChart from "../../../../components/GradeChart";
import { useHistory } from "react-router-dom";
import fetchCourses from "../../_apis/CoursesApi";

// styles
const useStyles = makeStyles((theme) => ({
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none",
    },
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbar: {
    flexWrap: "wrap",
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[200]
        : theme.palette.grey[700],
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
  },
  center: {
    position: "relative",
    display: "inline-block",
    marginLeft: "46%",
    left: 20,
    marginTop: "20%",
    top: 20,
  },
}));

// id to label conversion
const information = [
  {
    id: "instructor",
    label: "Instructor",
  },
  {
    id: "unique",
    label: "Unique ID",
  },
  {
    id: "semester",
    label: "Semester",
  },
  {
    id: "days",
    label: "Days",
  },
  {
    id: "hour",
    label: "Times",
  },
  {
    id: "req",
    label: "Requirements fulfilled",
  },
];

export default function Course({ filters: { by, string } }) {
  const classes = useStyles();
  const [data, setData] = useState();

  const history = useHistory();

  // handles linking
  const handleProfessorClick = (instructorName) => {
    if (instructorName !== "NONE")
      history.push(`/professors/name/${encodeURIComponent(instructorName)}`);
  };

  const handleRequirementClick = (requirementName) => {
    // can't click a none link
    if (requirementName !== "NONE")
      history.push(`/requirements/name/${encodeURIComponent(requirementName)}`);
  };

  useEffect(() => {
    const getData = async () => {
      // API call
      const course = await fetchCourses(by, string);
      setData(course.data[0]);
    };
    getData();
  });
  return (
    <>
      {/* default if class not found */}
      {!data ? (
        <CircularProgress className={classes.center} />
      ) : (
        <>
          <CssBaseline />
          <Container
            maxWidth="sm"
            component="main"
            className={classes.heroContent}
          >
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="textPrimary"
              gutterBottom
            >
              {data.name}
            </Typography>
          </Container>
          <Container maxWidth="lg" component="main">
            <Grid container spacing={5} alignItems="flex-end">
              {/* Display information */}
              {information.map((info) => (
                <Grid
                  item
                  key={info.label}
                  md={
                    info.label === "Times"
                      ? 6
                      : info.label === "Days" ||
                        info.label === "Requirements fulfilled"
                      ? 3
                      : 4
                  }
                >
                  <Card>
                    <CardHeader
                      title={info.label}
                      subheader={info.subheader}
                      titleTypographyProps={{ align: "center" }}
                      subheaderTypographyProps={{ align: "center" }}
                      action={info.label === "Pro" ? <StarIcon /> : null}
                      className={classes.cardHeader}
                    />
                    <CardContent>
                      <div className={classes.cardPricing}>
                        <Typography
                          style={{ alignSelf: "center" }}
                          component="h2"
                          variant="h3"
                          color="textPrimary"
                        >
                          {/* make clickable versions for instructor and requirement */}
                          {info.label === "Instructor"
                            ? data[info.id].map((instructorName) => (
                                <p
                                  onClick={() =>
                                    handleProfessorClick(instructorName)
                                  }
                                >
                                  {instructorName}
                                </p>
                              ))
                            : info.label === "Requirements fulfilled"
                            ? data[info.id]
                                .split("\\N")
                                .map((requirementName) => (
                                  <p
                                    onClick={() =>
                                      handleRequirementClick(requirementName)
                                    }
                                  >
                                    {requirementName}
                                  </p>
                                ))
                            : data[info.id]}
                        </Typography>
                      </div>
                      <ul>
                        {info.description &&
                          info.description.map((line) => (
                            <Typography
                              component="li"
                              variant="subtitle1"
                              align="center"
                              key={line}
                            >
                              {line}
                            </Typography>
                          ))}
                      </ul>
                    </CardContent>
                  </Card>
                </Grid>
              ))}
            </Grid>
            <GradeChart />
          </Container>
        </>
      )}
    </>
  );
}
