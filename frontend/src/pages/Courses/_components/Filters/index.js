import React, { useState } from "react";
import Button from "@material-ui/core/Button";

/*  Filter Displays  */
import RadioButtons from "../../../../components/FilterDisplays/RadioButtons";
import Search from "../../../../components/FilterDisplays/Search";
import SingleSelectDropdown from "../../../../components/FilterDisplays/SingleSelectDropdown";

// column names and properties
const columns = {
  pkey: {
    label: "Key",
    type: "search",
    placeholder: "Search key numbers...",
    default: "",
  },
  name: {
    label: "Name",
    type: "search",
    placeholder: "Search Name...",
    default: "",
  },
  unique: {
    label: "Unique Number",
    type: "search",
    placeholder: "Search Unique Number...",
    default: "",
  },
  department: {
    label: "Department",
    type: "search",
    placeholder: "Search Department...",
    default: "",
  },
  number: {
    label: "Course Number",
    type: "search",
    placeholder: "Search Course Number...",
    default: "",
  },
  semester: {
    label: "Semester",
    type: "dropdown",
    options: [
      "All",
      "Spring 2016",
      "Summer 2016",
      "Fall 2016",
      "Spring 2017",
      "Summer 2017",
      "Fall 2017",
      "Spring 2018",
      "Summer 2018",
      "Fall 2018",
      "Spring 2019",
      "Summer 2019",
      "Fall 2019",
      "Spring 2020",
      "Summer 2020",
      "Fall 2020",
    ],
    default: "All",
  },
  level: {
    label: "Course Level",
    type: "dropdown",
    options: ["All", "Lower", "Upper", "Graduate"],
    default: "All",
  },
};

// handles filtering
export default function Filters(props) {
  const { onFilterChange } = props;

  const [filters, setFilters] = useState(
    Object.keys(columns).reduce(
      (obj, column) => ({ ...obj, [column]: columns[column].default }),
      {}
    )
  );
  const [activeFilter, setActiveFilter] = useState("");

  const handleFilterChange = (key, event) => {
    const newFilters = Object.assign({}, filters);
    if (activeFilter && activeFilter !== key)
      newFilters[activeFilter] = columns[activeFilter].default;
    newFilters[key] = event.target.value;
    setActiveFilter(key);
    setFilters(newFilters);
  };

  /*  applies changes to parent compoennt */
  const handleFilterUpdate = () => onFilterChange(activeFilter, filters);

  return (
    <>
      <div className="card">
        <h4 className="card-header text-left font-weight-bold">
          Course Filters
        </h4>
        <div className="card-body">
          {Object.keys(columns).map((i) => {
            switch (columns[i].type) {
              case "dropdown":
                return (
                  <>
                    <div>
                      <h5 className="card-title">{columns[i].label}</h5>
                      <SingleSelectDropdown
                        currentOption={filters[i]}
                        options={columns[i].options}
                        onChange={(event) => handleFilterChange(i, event)}
                      />
                    </div>
                    <br />
                  </>
                );
              case "radio":
                return (
                  <>
                    <div>
                      <h5 className="card-title">{columns[i].label}</h5>
                      <RadioButtons
                        currentOption={filters[i]}
                        options={columns[i].options}
                        onChange={(event) => handleFilterChange(i, event)}
                      />
                    </div>
                    <br />
                  </>
                );
              case "search":
                return (
                  <>
                    <div>
                      <h5 className="card-title">{columns[i].label}</h5>
                      <Search
                        input={filters[i]}
                        placeholder={columns[i].placeholder}
                        onChange={(event) => handleFilterChange(i, event)}
                      />
                    </div>
                    <br />
                  </>
                );
              default:
                return <p>Error</p>;
            }
          })}
        </div>
        {/* update filters on button click */}
        <Button variant="contained" onClick={handleFilterUpdate}>
          Update Filters
        </Button>
      </div>
    </>
  );
}
