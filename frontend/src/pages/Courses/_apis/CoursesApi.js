import http from "../../../API/HTTP";

const COURSES_URL = "/api/Courses/";
const constants = require("../constants.js");

// handles all api calls for courses
export const fetchCourses = async (
  filterBy = "",
  filterString = "",
  sortBy = "",
  reverse = "False",
  pageNumber = 1,
  pageSize = 1
) => {
  let response;
  if (filterString === "All") {
    filterString = "";
    filterBy = "";
    console.log("sup", decodeURIComponent(""));
  } else {
    if (filterBy === "level") filterString = constants.LEVELS_RAW[filterString];
    if (filterBy === "semester") {
      const semester = filterString.split(" ");
      filterString = `${semester[1]}${constants.SEMESTERS_RAW[semester[0]]}`;
    }
  }

  try {
    // API call
    response = await http.get(COURSES_URL, {
      params: {
        page_number: pageNumber,
        page_size: pageSize,
        filter_by: filterBy,
        filter_string: decodeURIComponent(filterString),
        sort_by: sortBy,
        reverse,
      },
    });
  } catch (err) {
    return {
      count: 0,
      data: [],
    };
  }
  const data = response.data;
  // format data
  if (data.data.length && constants.LEVELS_FORMATTED[data.data[0].level])
    data.data.forEach((datum) => {
      datum.level = constants.LEVELS_FORMATTED[datum.level];
      datum.semester = `${
        constants.SEMESTERS_FORMATTED[datum.semester.charAt(4)]
      } ${datum.semester.substring(0, 4)}`;
    });

  return data;
};

export default fetchCourses;
