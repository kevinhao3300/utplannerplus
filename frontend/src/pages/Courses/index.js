import React, { Component } from "react";
import Table from "../../components/Table";
import fetchCourses from "./_apis/CoursesApi";
import Filters from "./_components/Filters";

import Comparator from "../../components/Comparison/Comparator";
import ComparisonTable from "../../components/Comparison/Table";

/*  used for column formatting for data table  */
export const columns = [
  { id: "pkey", label: "Key #", minWidth: 50 },
  { id: "name", label: "Name", minWidth: 170 },
  {
    id: "unique",
    label: "Unique #",
    minWidth: 170,
  },
  {
    id: "department",
    label: "Department",
    minWidth: 100,
  },
  {
    id: "number",
    label: "Course #",
    minWidth: 100,
  },
  {
    id: "semester",
    label: "Semester",
    minWidth: 170,
  },
  {
    id: "level",
    label: "Level",
    minWidth: 50,
  },
];

/*  used to display information for given data points for comparison table  */
const rows = [
  { id: "pkey", label: "Key #", minWidth: 170 },
  { id: "name", label: "Name", minWidth: 170 },
  {
    id: "unique",
    label: "Unique #",
    minWidth: 170,
  },
  {
    id: "department",
    label: "Department",
    minWidth: 100,
  },
  {
    id: "number",
    label: "Course #",
    minWidth: 100,
  },
  {
    id: "semester",
    label: "Semester",
    minWidth: 170,
  },
  {
    id: "level",
    label: "Course Level",
    minWidth: 50,
  },
];
export class Courses extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: props.search
        ? { column: "name", input: { name: props.search } }
        : { column: "", input: {} },
      comparison: {
        visible: false,
        pkeys: [],
      },
    };
  }

  handleFilterChange = (column, input) => {
    this.setState({ filter: { column, input } });
  };

  handleComparatorChange = (visible, pkeys) => {
    this.setState({ comparison: { visible, pkeys } });
  };

  render() {
    const {
      filter,
      comparison: { visible, pkeys },
    } = this.state;
    return (
      <>
        <h2>Courses</h2>
        <h3>Click on the courses below to get more information about them.</h3>
        {/* Render requirements table */}
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-3">
              <Filters onFilterChange={this.handleFilterChange} />
              <br />
              <Comparator
                title={"Course"}
                onComparison={this.handleComparatorChange}
                count={120564}
              />
            </div>
            <div className="col-md-9">
              {!visible ? (
                <Table
                  columns={columns}
                  query={fetchCourses}
                  route={"courses"}
                  filter={filter}
                />
              ) : (
                <ComparisonTable
                  query={fetchCourses}
                  title={"Course"}
                  route={"courses"}
                  rows={rows}
                  pkeys={pkeys}
                  hide={this.handleComparatorChange}
                />
              )}
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Courses;
