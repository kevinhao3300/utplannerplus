export const SEMESTERS_FORMATTED = {
  All: "All",
  2: "Spring",
  6: "Summer",
  9: "Fall",
};

export const SEMESTERS_RAW = {
  All: "All",
  Spring: 2,
  Summer: 6,
  Fall: 9,
};

export const LEVELS_FORMATTED = {
  All: "All",
  L: "Lower",
  U: "Upper",
  G: "Graduate",
};

export const LEVELS_RAW = {
  All: "All",
  Lower: "L",
  Upper: "U",
  Graduate: "G",
};
