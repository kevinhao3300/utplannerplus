import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import { fade, makeStyles } from "@material-ui/core/styles";
import STYLES from "../../constants/Fonts";
import { useHistory } from "react-router-dom";
import SearchBar from "../../components/SearchBar";

// styles
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  title: {
    fontFamily: STYLES.TITLE.FONT,
    fontSize: STYLES.TITLE.SIZE,
    color: "white",
    textDecoration: "none",
    alignSelf: "center",
  },
  subtext: {
    fontFamily: STYLES.HEADING.FONT,
    fontSize: STYLES.HEADING.SIZE,
    color: "white",
    textDecoration: "italic",
  },
  container: {
    width: "100%",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    opacity: "90%",
  },
  search: {
    display: "flex",
    position: "relative",
    padding: 2,
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "relative",
    pointerEvents: "none",
    display: "flex",
  },
  inputRoot: {
    color: "inherit",
  },
}));

export function Home() {
  const classes = useStyles();
  const history = useHistory();
  const [searchText, setSearchText] = useState("");
  // search functionality
  const submitSearch = (event) => {
    if (event.key === "Enter" && searchText) {
      history.push(`/search/${searchText}`);
    }
  };
  return (
    <>
      {/* home image */}
      <img
        className={classes.container}
        src={require("../../images/ut/sunset_tower.jpg")}
        alt={"Splash"}
      />
      <div
        className="absolute-centered-box"
        style={{
          backgroundColor: "rgba(191, 87, 0, 0.5)",
          height: "50%",
          width: "50%",
        }}
      >
        {/* Home page title */}
        <Typography className={classes.title} variant="h2">
          UT Planner Plus
        </Typography>

        {/* Search bar */}
        <SearchBar
          placeholder="Search…"
          value={searchText}
          onChange={(e) => setSearchText(e.target.value)}
          onSubmit={submitSearch}
        />
      </div>
    </>
  );
}

export default Home;
