import React, { useState } from "react";
import { fade, makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Table from "../../components/Table";
import CardGrid from "../../components/CardGrid";

import { columns as courseColumns } from "../Courses";
import fetchCourses from "../Courses/_apis/CoursesApi";
import {
  columns as professorColumns,
  createProfessorCard,
} from "../Professors";
import fetchProfessors from "../Professors/_apis/ProfessorsApi";
import { columns as requirementColumns } from "../Requirements";
import fetchRequirements from "../Requirements/_apis/RequirementsApi";

import { useHistory } from "react-router-dom";
import SearchBar from "../../components/SearchBar";
// handle tabbing
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

// tab formatting and control
TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

// styles
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  search: {
    display: "flex",
    position: "relative",
    padding: 2,
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "relative",
    pointerEvents: "none",
    display: "flex",
  },
  inputRoot: {
    color: "inherit",
  },
}));

export function Search({ searchText }) {
  const styles = useStyles();
  const history = useHistory();
  // holds search text
  const [input, setInput] = useState(searchText);
  const filter = { column: "all", input: { all: searchText } };

  // handles tab value
  const [tabValue, setTabValue] = useState(0);

  // handles tab changes
  const handleTabChange = (event, newValue) => {
    setTabValue(newValue);
  };
  // search functionality
  const submitSearch = (event) => {
    if (event.key === "Enter" && input) {
      history.push(`/search/${input}`);
    }
  };
  return (
    <>
      <h1>Search Page</h1>
      {/* Search bar */}
      <SearchBar
        placeholder="Search…"
        onChange={(e) => setInput(e.target.value)}
        onSubmit={submitSearch}
      />
      {/* Add tab system to look through different models */}
      <div className={styles.root}>
        <AppBar position="static">
          <Tabs
            value={tabValue}
            onChange={handleTabChange}
            aria-label="simple tabs example"
          >
            <Tab label="Classes" {...a11yProps(0)} />
            <Tab label="Professors" {...a11yProps(1)} />
            <Tab label="Requirements" {...a11yProps(2)} />
          </Tabs>
        </AppBar>
        <TabPanel value={tabValue} index={0}>
          {/* Render classes table */}
          <Table
            columns={courseColumns}
            query={fetchCourses}
            route={"courses"}
            filter={filter}
          />
        </TabPanel>
        <TabPanel value={tabValue} index={1}>
          {/* Render professors grid */}
          <CardGrid
            columns={professorColumns}
            query={fetchProfessors}
            route={"professors"}
            filter={filter}
            cardComponent={createProfessorCard}
          />
        </TabPanel>
        <TabPanel value={tabValue} index={2}>
          {/* Render requirements table */}
          <Table
            columns={requirementColumns}
            query={fetchRequirements}
            route={"requirements"}
            filter={filter}
          />
        </TabPanel>
      </div>
    </>
  );
}

export default Search;
