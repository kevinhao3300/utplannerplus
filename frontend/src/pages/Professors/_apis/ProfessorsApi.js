import http from "../../../API/HTTP";
const PROFESSORS_URL = "/api/Professors/";

// handles all API requests involving professors
export const fetchProfessors = async (
  filterBy = "",
  filterString = "",
  sortBy = "",
  reverse = "False",
  pageNumber = 1,
  pageSize = 1
) => {
  let response;
  try {
    // API call
    response = await http.get(PROFESSORS_URL, {
      params: {
        page_number: pageNumber,
        page_size: pageSize,
        filter_by: filterBy,
        filter_string: decodeURIComponent(filterString),
        sort_by: sortBy,
        reverse,
      },
    });
  } catch (err) {
    return {
      count: 0,
      data: [],
    };
  }
  const data = response.data;
  return data;
};

export default fetchProfessors;
