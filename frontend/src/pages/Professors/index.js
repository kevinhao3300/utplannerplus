import React, { Component } from "react";
import CardGrid from "../../components/CardGrid";
import fetchProfessors from "./_apis/ProfessorsApi";
import Filters from "./_components/Filters";

import Comparator from "../../components/Comparison/Comparator";
import ComparisonTable from "../../components/Comparison/Table";

import ProfessorCard from "../../pages/Professors/_components/ProfessorCard";

export const columns = [
  { id: "pkey", label: "Key" },
  { id: "name", label: "Name" },
  { id: "department", label: "Department" },
  { id: "numRatings", label: "Number of Ratings" },
  { id: "ratingClass", label: "Overall Rating" },
  { id: "overallRating", label: "rating" },
];

const rows = [
  { id: "pkey", label: "Key" },
  { id: "name", label: "Name" },
  { id: "department", label: "Department" },
  { id: "numRatings", label: "Number of Ratings" },
  { id: "ratingClass", label: "Overall Rating" },
  { id: "overallRating", label: "Rating" },
  { id: "levelOfDifficulty", label: "Difficulty level" },
  { id: "wouldTakeAgain", label: "Would take again" },
];

export const createProfessorCard = (professor, filters) => {
  return <ProfessorCard professor={professor} filter={filters} />;
};
export class Professors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: { column: "", input: {} },
      comparison: {
        visible: false,
        pkeys: [],
      },
    };
  }
  handleFilterChange = (column, input) => {
    this.setState({ filter: { column, input } });
  };

  handleComparatorChange = (visible, pkeys) => {
    this.setState({ comparison: { visible, pkeys } });
  };

  render() {
    const {
      filter,
      comparison: { visible, pkeys },
    } = this.state;
    return (
      <>
        <h2>Professors</h2>
        <h3>
          Click on the professors below to get more information about them.
        </h3>
        {/* Render requirements table */}
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-3">
              <Filters onFilterChange={this.handleFilterChange} />
              <br />
              <Comparator
                title={"Professor"}
                onComparison={this.handleComparatorChange}
                count={7127}
              />
            </div>
            <div className="col-md-9">
              {/* Render grid of professors */}
              {!visible ? (
                <CardGrid
                  columns={columns}
                  query={fetchProfessors}
                  route={"professors"}
                  filter={filter}
                  cardComponent={createProfessorCard}
                />
              ) : (
                <ComparisonTable
                  query={fetchProfessors}
                  title={"Professor"}
                  route={"professors"}
                  rows={rows}
                  pkeys={pkeys}
                  hide={this.handleComparatorChange}
                />
              )}
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Professors;
