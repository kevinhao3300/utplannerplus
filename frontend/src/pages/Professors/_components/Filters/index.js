import React, { useState } from "react";
import Button from "@material-ui/core/Button";

/*  Filter Displays  */
import RadioButtons from "../../../../components/FilterDisplays/RadioButtons";
import Search from "../../../../components/FilterDisplays/Search";
import SingleSelectDropdown from "../../../../components/FilterDisplays/SingleSelectDropdown";

const columns = {
  pkey: {
    label: "Key",
    type: "search",
    placeholder: "Search key numbers...",
    default: "",
  },
  name: {
    label: "Name",
    type: "search",
    placeholder: "Search names...",
    default: "",
  },
  department: {
    label: "Department",
    type: "search",
    placeholder: "Search departments...",
    default: "",
  },
  overallRating: {
    label: "Overall Rating",
    type: "search",
    placeholder: "Search hours...",
    default: "",
  },
  numRatings: {
    label: "# of Ratings",
    type: "search",
    placeholder: "Search # of Ratings...",
    default: "",
  },
  ratingClass: {
    label: "Rating Class",
    type: "search",
    placeholder: "Search Rating Class...",
    default: "",
  },
};

export default function Filters(props) {
  const { onFilterChange } = props;

  const [filters, setFilters] = useState(
    Object.keys(columns).reduce(
      (obj, column) => ({ ...obj, [column]: columns[column].default }),
      {}
    )
  );
  const [activeFilter, setActiveFilter] = useState("");

  const handleFilterChange = (key, event) => {
    const newFilters = Object.assign({}, filters);
    if (activeFilter && activeFilter !== key)
      newFilters[activeFilter] = columns[activeFilter].default;
    newFilters[key] = event.target.value;
    setActiveFilter(key);
    setFilters(newFilters);
  };

  /*  applies changes to parent compoennt */
  const handleFilterUpdate = () => onFilterChange(activeFilter, filters);

  return (
    <>
      <div className="card">
        <h4 className="card-header text-left font-weight-bold">
          Professor Filters
        </h4>
        <div className="card-body">
          {Object.keys(columns).map((i) => {
            switch (columns[i].type) {
              case "dropdown":
                return (
                  <>
                    <div>
                      <h5 className="card-title">{columns[i].label}</h5>
                      <SingleSelectDropdown
                        currentOption={filters[i]}
                        options={columns[i].options}
                        onChange={(event) => handleFilterChange(i, event)}
                      />
                    </div>
                    <br />
                  </>
                );
              case "radio":
                return (
                  <>
                    <div>
                      <h5 className="card-title">{columns[i].label}</h5>
                      <RadioButtons
                        currentOption={filters[i]}
                        options={columns[i].options}
                        onChange={(event) => handleFilterChange(i, event)}
                      />
                    </div>
                    <br />
                  </>
                );
              case "search":
                return (
                  <>
                    <div>
                      <h5 className="card-title">{columns[i].label}</h5>
                      <Search
                        input={filters[i]}
                        placeholder={columns[i].placeholder}
                        onChange={(event) => handleFilterChange(i, event)}
                      />
                    </div>
                    <br />
                  </>
                );
              default:
                return <p>Error</p>;
            }
          })}
        </div>
        <Button variant="contained" onClick={handleFilterUpdate}>
          Update Filters
        </Button>
      </div>
    </>
  );
}
