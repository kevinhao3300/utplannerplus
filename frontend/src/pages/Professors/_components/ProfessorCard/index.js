import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Highlighter from "react-highlight-words";

// styles
const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "130%", // 16:9
    // height: "56.25%"
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));

const fields = {
  pkey: "Key",
  department: "Department",
  overallRating: "Overall Rating",
  numRatings: "# of Ratings",
  ratingClass: "Rating Class",
};

export default function ProfessorCard(props) {
  const { professor, filter = {} } = props;
  const classes = useStyles();

  // render professor card
  return (
    <Grid item key={professor} xs={12} sm={6} md={4}>
      <Card className={classes.card}>
        <CardMedia
          className={classes.cardMedia}
          image={
            // default image
            professor.image === "https://imgur.com/a/LsZkpfi"
              ? "https://denovo-us.com/wp-content/uploads/2018/05/Male-Generic-Photo-01-e1526490708781.jpg"
              : professor.image
          }
          title={professor.name}
        />
        <CardContent className={classes.cardContent}>
          <h4>
            <a href={`/professors/pkey/${professor.pkey}`}>
              {(filter.column === "name" && filter.input[filter.column]) ||
              filter.column === "all" ? (
                <Highlighter
                  searchWords={filter.input[filter.column].split(" ")}
                  autoEscape={false}
                  textToHighlight={professor.name}
                />
              ) : (
                <span>{professor.name}</span>
              )}
            </a>
          </h4>
          {Object.keys(fields).map((field) => {
            return (
              <div>
                {fields[field]}:{" "}
                <span style={{ float: "right" }}>
                  {(filter.column === field && filter.input[filter.column]) ||
                  filter.column === "all" ? (
                    // highlighting
                    <Highlighter
                      searchWords={
                        filter.column === "all"
                          ? filter.input[filter.column].split(" ")
                          : filter.input[field].split(" ")
                      }
                      autoEscape={false}
                      textToHighlight={professor[field].toString()}
                    />
                  ) : (
                    professor[field].toString()
                  )}
                </span>
              </div>
            );
          })}
        </CardContent>
      </Card>
    </Grid>
  );
}
