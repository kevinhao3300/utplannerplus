import React, { useState, useEffect } from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import StarIcon from "@material-ui/icons/StarBorder";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import CircularProgress from "@material-ui/core/CircularProgress";
import CardMedia from "@material-ui/core/CardMedia";
import { useHistory } from "react-router-dom";

import fetchProfessors from "../../_apis/ProfessorsApi";
import fetchCourses from "../../../Courses/_apis/CoursesApi";
import Table from "../../../../components/Table";
import { columns as col } from "../../../Courses";
const columns = [...col];
columns.push({ id: "req", label: "Requirements" });
const useStyles = makeStyles((theme) => ({
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none",
    },
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbar: {
    flexWrap: "wrap",
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[200]
        : theme.palette.grey[700],
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
  },
  center: {
    position: "relative",
    display: "inline-block",
    marginLeft: "46%",
    left: 20,
    marginTop: "20%",
    top: 20,
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
}));

// professor information
const information = [
  {
    id: "overallRating",
    label: "Rating",
  },
  {
    id: "numRatings",
    label: "Number of Ratings",
  },
  {
    id: "department",
    label: "Department",
  },
  {
    id: "levelOfDifficulty",
    label: "Difficulty",
  },
];

const Professor = (props) => {
  const {
    filters: { by, string },
  } = props;
  const classes = useStyles();
  const history = useHistory();
  const [data, setData] = useState();
  useEffect(() => {
    const getData = async () => {
      // API call
      const professor = await fetchProfessors(by, string);
      setData(professor.data[0]);
    };
    getData();
  });

  const transformRequirements = (requirementsString) => {
    let requirementsList;
    // reformat data
    if (requirementsString.includes(", "))
      requirementsList = requirementsString.split(", ");
    else requirementsList = requirementsString.split("\\N");

    return (
      <ul>
        {requirementsList.map((requirementInstance) => (
          <li
            // link to another model
            onClick={() => history.push(`/requirements/${requirementInstance}`)}
          >
            {requirementInstance}
          </li>
        ))}
      </ul>
    );
  };

  return (
    <>
      {!data ? (
        <CircularProgress className={classes.center} />
      ) : (
        <>
          <CssBaseline />
          <Container
            maxWidth="sm"
            component="main"
            className={classes.heroContent}
          >
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="textPrimary"
              gutterBottom
            >
              {data.name}
            </Typography>
            <CardMedia
              style={{ height: 600, width: 600 }}
              className={classes.cardMedia}
              image={
                // default image
                data.image === "https://imgur.com/a/LsZkpfi"
                  ? "https://denovo-us.com/wp-content/uploads/2018/05/Male-Generic-Photo-01-e1526490708781.jpg"
                  : data.image
              }
            />
          </Container>
          <Container maxWidth="lg" component="main">
            <Grid container spacing={5} alignItems="center">
              {/* display information */}
              {information.map((info) => (
                <Grid
                  item
                  key={info.id}
                  md={
                    info.label === "Classes Taught" ||
                    info.label === "Requirements taught"
                      ? 6
                      : 3
                  }
                >
                  <Card>
                    <CardHeader
                      title={info.label}
                      titleTypographyProps={{ align: "center" }}
                      subheaderTypographyProps={{ align: "center" }}
                      action={
                        info.label === "Classes Taught" ? <StarIcon /> : null
                      }
                      className={classes.cardHeader}
                    />
                    <CardContent>
                      <div className={classes.cardPricing}>
                        <Typography
                          component="h2"
                          variant="h3"
                          color="textPrimary"
                        >
                          {info.label === "Classes Taught"
                            ? Array.from(
                                new Set(data.classes_taught.split("/"))
                              ).length
                            : info.label === "Requirements taught"
                            ? transformRequirements(data[info.id])
                            : data[info.id]}
                        </Typography>
                      </div>
                      <ul>
                        {info.label === "Classes Taught" &&
                          Array.from(
                            new Set(data.classes_taught.split(" / "))
                          ).map((line) => (
                            <Typography
                              component="li"
                              variant="sublabel1"
                              align="center"
                              key={line}
                              onClick={() => {
                                history.push(
                                  `/classes/${encodeURIComponent(line)}`
                                );
                              }}
                            >
                              {line}
                            </Typography>
                          ))}
                      </ul>
                    </CardContent>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </Container>
          {/* render table of information */}
          <Table
            columns={columns}
            query={fetchCourses}
            route={"courses"}
            filter={{ column: "instructor", input: { instructor: data.name } }}
          />
        </>
      )}
    </>
  );
};

export default Professor;
