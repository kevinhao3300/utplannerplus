import React, { Component } from "react";

import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import Cases from "./_components/COVID/Cases";
import PopulationDensity from "./_components/COVID/PopulationDensity";
import GDP from "./_components/COVID/GDP";

import Levels from "./_components/Planner/Levels";
import Departments from "./_components/Planner/Departments";
import Hours from "./_components/Planner/Hours";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && children}
    </div>
  );
}

// tab formatting and control
TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const COVID_TABS = [
  {
    title: "Countries",
    component: <PopulationDensity />,
    style: {
      height: "2250px",
      width: "90%",
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  {
    title: "Case Statistics",
    component: <Cases />,
    style: {
      height: "750px",
      width: "90%",
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  {
    title: "GDP",
    component: <GDP />,
    style: {
      height: "1500px",
      width: "90%",
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
];

const PLANNER_TABS = [
  {
    title: "Courses",
    component: <Levels />,
    style: {
      height: "750px",
      width: "90%",
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  {
    title: "Professors",
    component: <Departments />,
    style: {
      height: "1000px",
      width: "100%",
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  {
    title: "Requirements",
    component: <Hours />,
    style: {
      height: "750px",
      width: "100%",
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
];
export class Visualizations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mainTab: 0,
      covidTab: 0,
      plannerTab: 0,
    };
  }
  handleTabChange = (event, newValue) => {
    this.setState({ mainTab: newValue });
  };

  handleCovidTabChange = (event, newValue) => {
    this.setState({ covidTab: newValue });
  };

  handlePlannerTabChange = (event, newValue) => {
    this.setState({ plannerTab: newValue });
  };
  render() {
    const { mainTab, covidTab, plannerTab } = this.state;
    return (
      <>
        <AppBar position="static">
          <Tabs value={mainTab} onChange={this.handleTabChange}>
            <Tab label="UT Planner Plus" />
            <Tab label="COVID-19" />
          </Tabs>
        </AppBar>
        <TabPanel value={mainTab} index={0}>
          {/* Render professors grid */}
          <AppBar position="static">
            <Tabs value={plannerTab} onChange={this.handlePlannerTabChange}>
              {PLANNER_TABS.map((tab) => (
                <Tab label={tab.title} />
              ))}
            </Tabs>
          </AppBar>
          {PLANNER_TABS.map((tab, i) => {
            return (
              <TabPanel value={plannerTab} index={i} key={i}>
                <div style={tab.style}>{tab.component}</div>
              </TabPanel>
            );
          })}
        </TabPanel>
        <TabPanel value={mainTab} index={1}>
          <AppBar position="static">
            <Tabs value={covidTab} onChange={this.handleCovidTabChange}>
              {COVID_TABS.map((tab) => (
                <Tab label={tab.title} />
              ))}
            </Tabs>
          </AppBar>
          {COVID_TABS.map((tab, i) => {
            return (
              <TabPanel value={covidTab} index={i} key={i}>
                <div style={tab.style}>{tab.component}</div>
              </TabPanel>
            );
          })}
        </TabPanel>
      </>
    );
  }
}

// courses - distribution of level over semesters
// professors - distribution of professor amounts over department
// requirements - some shit
