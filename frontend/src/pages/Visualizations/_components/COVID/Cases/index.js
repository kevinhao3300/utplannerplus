import React, { useState, useEffect } from "react";
import { ResponsiveScatterPlot } from "@nivo/scatterplot";
import http from "../../../../../API/HTTP";
import CircularProgress from "@material-ui/core/CircularProgress";

const Cases = () => {
  let [data, setData] = useState([]);
  useEffect(() => {
    const getData = async () => {
      const raw = await http.get("https://api.covid19db.net/case-statistics", {
        params: {
          attributes: "country,totals",
        },
      });
      const cases = [{ id: "countries", data: [] }];
      raw.data.forEach((datum) => {
        cases[0].data.push({
          x: datum.totals.cases,
          y: datum.totals.recovered,
          country: datum.country.name,
        });
      });
      setData(cases);
    };
    getData();
  }, []);

  return (
    <>
      {!data.length ? (
        <CircularProgress
          style={{
            position: "relative",
            display: "inline-block",
            marginLeft: "46%",
            left: 20,
            marginTop: "20%",
            top: 20,
          }}
        />
      ) : (
        <>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <h1>Number of Cases vs Number of Recoveries</h1>
          </div>
          <ResponsiveScatterPlot
            data={data}
            margin={{ top: 60, right: 140, bottom: 70, left: 90 }}
            xScale={{ type: "linear", min: 0, max: "auto" }}
            yScale={{ type: "linear", min: 0, max: "auto" }}
            blendMode="multiply"
            // Custom tooltip
            tooltip={({ node }) => {
              return (
                <div>
                  {node.data.country} ({node.data.x.toLocaleString()} Cases,{" "}
                  {node.data.y.toLocaleString()} Recovered)
                </div>
              );
            }}
            axisTop={null}
            axisRight={null}
            axisBottom={{
              orient: "bottom",
              tickSize: 5,
              tickPadding: 5,
              tickRotation: 0,
              legend: "Cases",
              legendPosition: "middle",
              legendOffset: 46,
            }}
            axisLeft={{
              orient: "left",
              tickSize: 5,
              tickPadding: 5,
              tickRotation: 0,
              legend: "Recovered",
              legendPosition: "middle",
              legendOffset: -60,
            }}
          />
        </>
      )}
    </>
  );
};

export default Cases;
