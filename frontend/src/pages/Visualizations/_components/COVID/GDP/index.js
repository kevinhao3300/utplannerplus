import React, { useEffect, useState } from "react";
import { ResponsivePie } from "@nivo/pie";
import http from "../../../../../API/HTTP";
import CircularProgress from "@material-ui/core/CircularProgress";

const GDP = () => {
  const [data, setData] = useState([]);
  // request data from API
  useEffect(() => {
    const getData = async () => {
      const risks = await http.get(
        "https://api.covid19db.net/risk-factor-statistics",
        {
          params: {
            attributes: "country,gdpPerCapita",
          },
        }
      );
      const gdp = risks.data.map((risk) => {
        return {
          id: risk.country.name,
          label: risk.country.name,
          value: risk.gdpPerCapita,
        };
      });
      setData(gdp);
    };
    getData();
  }, []);
  // show spinner if data not retrieved yet
  return (
    <>
      {!data.length ? (
        <CircularProgress
          style={{
            position: "relative",
            display: "inline-block",
            marginLeft: "46%",
            left: 20,
            marginTop: "20%",
            top: 20,
          }}
        />
      ) : (
        <>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <h1>GDP per Capita in Each Country</h1>
          </div>
          <ResponsivePie
            data={data}
            margin={{ top: 40, right: 100, bottom: 80, left: 100 }}
            innerRadius={0.5}
            padAngle={0.2}
            cornerRadius={3}
            colors={{ scheme: "nivo" }}
            borderWidth={1}
            borderColor={{ from: "color", modifiers: [["darker", 0.2]] }}
            radialLabelsTextColor="#333333"
            radialLabelsLinkColor={{ from: "color" }}
            radialLabelsSkipAngle={3}
            sliceLabelsSkipAngle={5}
            defs={[
              {
                id: "dots",
                type: "patternDots",
                background: "inherit",
                color: "rgba(255, 255, 255, 0.3)",
                size: 4,
                padding: 1,
                stagger: true,
              },
              {
                id: "lines",
                type: "patternLines",
                background: "inherit",
                color: "rgba(255, 255, 255, 0.3)",
                rotation: -45,
                lineWidth: 6,
                spacing: 10,
              },
            ]}
          />
        </>
      )}
    </>
  );
};

export default GDP;
