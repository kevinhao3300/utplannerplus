import React, { useState, useEffect } from "react";
import { ResponsiveBar } from "@nivo/bar";
import http from "../../../../../API/HTTP";
import CircularProgress from "@material-ui/core/CircularProgress";

const customTooltip = (node) => {
  return (
    <div>
      <strong>{node.data.id}</strong>
      {`: ${node.data.value}`}
    </div>
  );
};

const PopulationDensity = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    const getData = async () => {
      // API call
      const countries = await http.get("https://api.covid19db.net/countries", {
        params: {
          attributes: "name,population,region,area",
        },
      });
      const densities = countries.data
        .map((country) => {
          if (country.name === "Palestine") {
            return {
              id: country.name,
              value: 2195,
              region: country.region,
            };
          }
          return {
            id: country.name,
            value: country.population / country.area,
            region: country.region,
          };
        })
        .sort(function (a, b) {
          return a.value - b.value;
        });
      setData(densities);
    };
    getData();
  });

  return (
    <>
      {!data.length ? (
        <CircularProgress
          style={{
            position: "relative",
            display: "inline-block",
            marginLeft: "46%",
            left: 20,
            marginTop: "20%",
            top: 20,
          }}
        />
      ) : (
        <>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <h1>Population Density in Each Country</h1>
          </div>
          <ResponsiveBar
            animate={false}
            enableGridX={true}
            enableGridY={false}
            data={data}
            layout={"horizontal"}
            margin={{ top: 20, right: 50, bottom: 80, left: 200 }}
            padding={0.15}
            maxValue={8000}
            labelSkipHeight={15}
            tooltip={customTooltip}
            colors={{ scheme: "nivo" }}
            colorBy="indexValue"
            axisBottom={{
              tickSize: 0,
              tickPadding: 5,
              legend: "Population Density (people per square mile)",
              legendPosition: "middle",
              legendOffset: 40,
              tickValues: 40,
            }}
            axisLeft={{
              tickPadding: 5,
              tickSize: 0,
              tickRotation: 0,
              legend: "Country",
              legendPosition: "middle",
              legendOffset: -100,
            }}
          />
        </>
      )}
    </>
  );
};
export default PopulationDensity;
