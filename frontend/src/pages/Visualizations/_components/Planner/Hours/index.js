import React, { useState, useEffect } from "react";
import { ResponsiveBar } from "@nivo/bar";
import fetchRequirements from "../../../../Requirements/_apis/RequirementsApi";
import CircularProgress from "@material-ui/core/CircularProgress";

const customTooltip = (node) => {
  return (
    <div>
      <strong>{node.data.id}</strong>
      {`: ${node.data.value}`}
    </div>
  );
};

const Hours = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    const getData = async () => {
      // API call
      const requirements = await fetchRequirements("", "", "", "False", 1, 23);
      const hours = requirements.data
        .map((requirement) => {
          return {
            id: requirement.name,
            value: requirement.numHours,
          };
        })
        .sort(function (a, b) {
          return a.value - b.value;
        });
      setData(hours);
    };
    getData();
  });

  return (
    <>
      {!data.length ? (
        <CircularProgress
          style={{
            position: "relative",
            display: "inline-block",
            marginLeft: "46%",
            left: 20,
            marginTop: "20%",
            top: 20,
          }}
        />
      ) : (
        <>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <h1>Hours Needed for Each Requirement</h1>
          </div>
          <ResponsiveBar
            data={data}
            margin={{ top: 20, right: 50, bottom: 80, left: 50 }}
            padding={0.15}
            maxValue={24}
            labelSkipHeight={15}
            tooltip={customTooltip}
            colors={{ scheme: "nivo" }}
            colorBy="indexValue"
            axisLeft={{
              tickSize: 0,
              tickPadding: 5,
              legend: "Hours",
              legendPosition: "middle",
              legendOffset: -40,
              tickValues: 12,
            }}
            axisBottom={{
              tickPadding: 5,
              tickSize: 0,
              tickRotation: 0,
              legend: "Requirement",
              legendPosition: "middle",
              legendOffset: 40,
              format: (d) => {
                const limit = 7;
                return `${d.substr(0, limit)}...`;
              },
            }}
          />
        </>
      )}
    </>
  );
};
export default Hours;
