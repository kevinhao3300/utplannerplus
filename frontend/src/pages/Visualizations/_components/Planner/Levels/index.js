import React, { useState, useEffect } from "react";
import { ResponsiveLine } from "@nivo/line";
import fetchCourses from "../../../../Courses/_apis/CoursesApi";
import CircularProgress from "@material-ui/core/CircularProgress";

const Levels = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    const semesters = {
      "Spring 2016": {},
      "Summer 2016": {},
      "Fall 2016": {},
      "Spring 2017": {},
      "Summer 2017": {},
      "Fall 2017": {},
      "Spring 2018": {},
      "Summer 2018": {},
      "Fall 2018": {},
      "Spring 2019": {},
      "Summer 2019": {},
      "Fall 2019": {},
      "Spring 2020": {},
      "Summer 2020": {},
      "Fall 2020": {},
    };

    const levels = [
      { id: "Lower", data: [] },
      { id: "Upper", data: [] },
      { id: "Graduate", data: [] },
    ];
    const getData = async () => {
      // API call
      const courses = await fetchCourses(
        "department",
        "C S",
        "",
        "False",
        1,
        2000
      );
      courses.data.forEach((course) => {
        semesters[course.semester][course.level] = semesters[course.semester][
          course.level
        ]
          ? semesters[course.semester][course.level] + 1
          : 1;
      });
      Object.keys(semesters).forEach((semester) => {
        levels.forEach((level) => {
          level.data.push({ x: semester, y: semesters[semester][level.id] });
        });
      });
      setData(levels);
    };
    getData();
  }, []);

  return (
    <>
      {!data.length ? (
        <CircularProgress
          style={{
            position: "relative",
            display: "inline-block",
            marginLeft: "46%",
            left: 20,
            marginTop: "20%",
            top: 20,
          }}
        />
      ) : (
        <>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <h1>Distribution of CS Class Levels Over Semesters</h1>
          </div>
          <ResponsiveLine
            data={data}
            margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
            xScale={{ type: "point" }}
            yScale={{
              type: "linear",
              min: "auto",
              max: "auto",
              reverse: false,
            }}
            yFormat=" >-.2f"
            axisTop={null}
            axisRight={null}
            axisBottom={{
              orient: "bottom",
              tickSize: 5,
              tickPadding: 5,
              tickRotation: 0,
              legend: "Semester",
              legendOffset: 36,
              legendPosition: "middle",
            }}
            axisLeft={{
              orient: "left",
              tickSize: 5,
              tickPadding: 5,
              tickRotation: 0,
              legend: "Total Classes",
              legendOffset: -40,
              legendPosition: "middle",
            }}
            pointSize={10}
            pointColor={{ theme: "background" }}
            pointBorderWidth={2}
            pointBorderColor={{ from: "serieColor" }}
            pointLabelYOffset={-12}
            useMesh={true}
            legends={[
              {
                anchor: "bottom-right",
                direction: "column",
                justify: false,
                translateX: 100,
                translateY: 0,
                itemsSpacing: 0,
                itemDirection: "left-to-right",
                itemWidth: 80,
                itemHeight: 20,
                itemOpacity: 0.75,
                symbolSize: 12,
                symbolShape: "circle",
                symbolBorderColor: "rgba(0, 0, 0, .5)",
                effects: [
                  {
                    on: "hover",
                    style: {
                      itemBackground: "rgba(0, 0, 0, .03)",
                      itemOpacity: 1,
                    },
                  },
                ],
              },
            ]}
          />
        </>
      )}
    </>
  );
};

export default Levels;
