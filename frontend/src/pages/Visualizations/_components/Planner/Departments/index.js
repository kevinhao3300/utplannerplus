import React, { useState, useEffect } from "react";
import { ResponsivePie } from "@nivo/pie";
import fetchProfessors from "../../../../Professors/_apis/ProfessorsApi";
import CircularProgress from "@material-ui/core/CircularProgress";

const Departments = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    const getData = async () => {
      // API call
      const raw = {};
      const professors = await fetchProfessors("", "", "", "False", 1, 8000);

      professors.data.forEach((professor) => {
        raw[professor.department] = raw[professor.department]
          ? raw[professor.department] + 1
          : 1;
      });
      const departments = Object.keys(raw).map((department) => {
        return {
          id: department,
          label: department,
          value: raw[department],
          color: `hsl(${Math.floor(Math.random() * 360)},${Math.floor(
            Math.random() * 100
          )},${Math.floor(Math.random() * 100)})`,
        };
      });
      setData(departments);
    };
    getData();
  }, []);

  return (
    <>
      {!data.length ? (
        <CircularProgress
          style={{
            position: "relative",
            display: "inline-block",
            marginLeft: "46%",
            left: 20,
            marginTop: "20%",
            top: 20,
          }}
        />
      ) : (
        <>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <h1>Number of Professors in Each Department</h1>
          </div>
          <ResponsivePie
            data={data}
            margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
            innerRadius={0.5}
            padAngle={0.3}
            cornerRadius={3}
            colors={{ scheme: "nivo" }}
            borderWidth={1}
            borderColor={{ from: "color", modifiers: [["darker", 0.2]] }}
            radialLabelsTextColor="#333333"
            radialLabelsLinkColor={{ from: "color" }}
            radialLabelsSkipAngle={2}
            sliceLabelsSkipAngle={4}
            defs={[
              {
                id: "dots",
                type: "patternDots",
                background: "inherit",
                color: "rgba(255, 255, 255, 0.3)",
                size: 4,
                padding: 1,
                stagger: true,
              },
              {
                id: "lines",
                type: "patternLines",
                background: "inherit",
                color: "rgba(255, 255, 255, 0.3)",
                rotation: -45,
                lineWidth: 6,
                spacing: 10,
              },
            ]}
          />
        </>
      )}
    </>
  );
};

export default Departments;
