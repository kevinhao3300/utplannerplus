import React from "react";
import App from "./App";
import { About } from "./pages/About";
import { Courses } from "./pages/Courses";
import { Home } from "./pages/Home";
import { Professors } from "./pages/Professors";
import { Requirements } from "./pages/Requirements";
import { Search } from "./pages/Search";
import { Professor } from "./pages/Professors/_components/Professor";
import Course from "./pages/Courses/_components/Course";
import { Requirement } from "./pages/Requirements/_components/Requirement";
import { DeveloperCard } from "./pages/About/_components/DeveloperCard";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";

it("App", () => {
  // renders initial app without crashing
  const tree = shallow(<App />);
  expect(toJson(tree)).toMatchSnapshot();
});

it("About", () => {
  // renders About page without crashing
  const tree = shallow(<About />);
  expect(toJson(tree)).toMatchSnapshot();
});

it("Classes", () => {
  // renders Classes page without crashing
  const tree = shallow(<Courses />);
  expect(toJson(tree)).toMatchSnapshot();
});

it("Home", () => {
  // renders Home page without crashing
  const tree = shallow(<Home />);
  expect(toJson(tree)).toMatchSnapshot();
});

it("Professors", () => {
  // renders Professors page without crashing
  const tree = shallow(<Professors />);
  expect(toJson(tree)).toMatchSnapshot();
});

it("Requirements", () => {
  // renders Requirements page without crashing
  const tree = shallow(<Requirements />);
  expect(toJson(tree)).toMatchSnapshot();
});

it("Search", () => {
  // renders Search page without crashing
  const tree = shallow(<Search />);
  expect(toJson(tree)).toMatchSnapshot();
});

it("Course", () => {
  // renders information about a course without crashing
  const tree = shallow(<Course filters={{ by: "", string: "" }} />);
  expect(toJson(tree)).toMatchSnapshot();
});

it("Requirement", () => {
  // renders information about a requirement without crashing
  const tree = shallow(<Requirement filters={{ by: "", string: "" }} />);
  expect(toJson(tree)).toMatchSnapshot();
});

it("DeveloperCard", () => {
  // renders information about the developers without crashing
  const tree = shallow(<DeveloperCard />);
  expect(toJson(tree)).toMatchSnapshot();
});
