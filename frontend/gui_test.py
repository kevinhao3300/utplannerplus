# need to run "npm start" on the frontend file in order to work

#requires:
#    pip3 install selenium
#    pip3 install webdriver-manager
#    pip3 install PyVirtualDisplay

from selenium import webdriver
import unittest
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
from pyvirtualdisplay import Display

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install()) 

    def test_title(self):
        driver = self.driver
        driver.get("http://localhost:3000")
        assert driver.title == 'UT Planner Plus'

    def test_name(self):
        driver = self.driver
        driver.get("http://localhost:3000")
        assert driver.name == 'chrome'

    def test_url_home(self):
        driver = self.driver
        driver.get("http://localhost:3000")
        assert driver.current_url == 'http://localhost:3000/'
        
    def test_homePage(self):
        driver = self.driver
        driver.get("http://localhost:3000")
        currHomeTab = driver.find_element_by_xpath("/html/body/div/header/div/div/div/div/a[1]")
        self.assertEqual(currHomeTab.get_attribute("textContent"), "about")
        currHomeTab = driver.find_element_by_xpath("/html/body/div/header/div/div/div/div/a[2]")
        self.assertEqual(currHomeTab.get_attribute("textContent"), "professors")
        currHomeTab = driver.find_element_by_xpath("/html/body/div/header/div/div/div/div/a[3]")
        self.assertEqual(currHomeTab.get_attribute("textContent"), "courses")
        currHomeTab = driver.find_element_by_xpath("/html/body/div/header/div/div/div/div/a[4]")
        self.assertEqual(currHomeTab.get_attribute("textContent"), "requirements")

    def test_aboutPage(self):
        driver = self.driver
        driver.get("http://localhost:3000")
        driver.find_elements_by_xpath("/html/body/div/header/div/div/div/div/a[1]")[0].click()
        data_btn = driver.find_elements_by_link_text("Data")[0]
        data_btn.click()
        data_box = driver.find_element_by_xpath("/html/body/div/div[3]/div[2]/div/div[1]")
        self.assertTrue("UT Registrar: We are going to scrape this link" in data_box.get_attribute("textContent"))
        data_btn = driver.find_elements_by_link_text("Tools")[0]
        data_btn.click()
        data_box = driver.find_element_by_xpath("/html/body/div/div[3]/div[2]/div/div[2]")
        self.assertTrue("GitLab: Allows us to use version control" in data_box.get_attribute("textContent"))
        data_btn = driver.find_elements_by_link_text("Other")[0]
        data_btn.click()
        data_box = driver.find_element_by_xpath("/html/body/div/div[3]/div[2]/div/div[3]")
        self.assertTrue("Postman URL: Our Postman URL" in data_box.get_attribute("textContent"))

    def test_professorsPage(self):
        driver = self.driver
        driver.get("http://localhost:3000")
        driver.find_elements_by_xpath("/html/body/div/header/div/div/div/div/a[2]")[0].click()

    def test_classesPage(self):
        driver = self.driver
        driver.get("http://localhost:3000")
        driver.find_elements_by_xpath("/html/body/div/header/div/div/div/div/a[3]")[0].click()

    def test_requirementsPage(self):
        driver = self.driver
        driver.get("http://localhost:3000")
        driver.find_elements_by_xpath("/html/body/div/header/div/div/div/div/a[4]")[0].click()

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()


